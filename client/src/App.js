import React, { useState, useEffect } from 'react';
import axios from 'axios'

import { Helmet } from 'react-helmet'

import { BrowserRouter as Router, Route, Redirect  } from "react-router-dom"
import './App.css';
import { url } from './config'


import EmployeeRegister from './AdministrationSite/adminComponents/employees/employee_register'
// import Administration from './AdministrationSite/containers/administration'


import Header from './containers/header'

//====== import sites visible to non-users ======

import NavBar from './components/navbar'

import Home from './containers/home'
import Who_Are_We from './containers/who_are_we'
import The_Team from './containers/the_team'
import Our_Locations from './containers/our_locations'
// import Our_History from './containers/our_history'
import Testimonials from './containers/testimonials'
import Contact from './containers/contact'
import Work_For_Us from './containers/work_for_us'

import EmployeeLogin from './containers/employeelogin'

//====== import sites visible only to logged-in users ======

import EmployeeNavbar from './AdministrationSite/adminComponents/employee_navbar'
import MyProfile from './AdministrationSite/adminContainers/my_profile'
import Events from './AdministrationSite/adminContainers/events'
import Employees from './AdministrationSite/adminContainers/employees'
import Miscellaneous from './AdministrationSite/adminContainers/miscellaneous'
import Jobtasks from './AdministrationSite/adminContainers/jobtasks'
import Offices from './AdministrationSite/adminContainers/offices'

// ====== Footer ======
import Footer from './containers/footer'




// ====== Whats beeing rendered on the page ======

function App() {

  // ============ Employee Login =============


  let [userInfo, setUserInfo] = useState({})
  let [isLoggedIn, setIsLoggedIn] = useState(false)
 

//function that change user content when loggedin and loggetout
  // useEffect when load page check if there is a token in localstorage
  // if token== null setUser to false 
  // : send token to server '/users/verify_token'
  // if ok : true change setUser to true else set to false 


const token = JSON.parse(localStorage.getItem('token'))

useEffect( () => {
  token === null 
  ? setUserInfo(false)
  : changeContent()
},[])

const changeContent = async () => {
 
  try{
     const response = await axios.post(`${url}/employees/verify_token`, {token})
  
    console.log("response in changeContent", response)
     return response.data.ok
     ? 
     (setIsLoggedIn(true), setUserInfo({
                 ...userInfo,
                 ...response.data.userData
                //also put books list in the state
              }) 
      ): setIsLoggedIn(false)
  }
  catch(error){
      console.log(error)
  }
}

// ====== Login/Logout ======
const login  = (token, user) => {

   localStorage.setItem('token',JSON.stringify(token)) 

    setUserInfo({
     ...userInfo,
     ...user
     }) 
   setIsLoggedIn(true)
}
const logout = () => {
   localStorage.removeItem('token');
   setIsLoggedIn(false)
}





  
  return (
    <Router>

     
        <Header/>

        <Route path='/employeeRegister'    component={EmployeeRegister}/>


        {
          isLoggedIn ?
          <EmployeeNavbar userInfo={userInfo} logout={logout}/>
          :<NavBar/>
        }
        {/* Routes visible to non-users */}
        <Route exact path="/"      component={Home} />
        <Route exact path="/who_are_we"      component={Who_Are_We} />
        <Route exact path="/the_team" component={The_Team} />
        <Route exact path="/our_locations" component={Our_Locations} />
        {/* <Route exact path="/our_history" component={Our_History} /> */}
        <Route exact path="/testimonials" component={Testimonials} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/work_for_us" component={Work_For_Us} />
        <Route exact path='/employee_login' render={ props => isLoggedIn
                                                          ? <Redirect to={'/employee_site/my_profile'} /> 
                                                          : <EmployeeLogin login={login} logout={logout}{...props}/>} />

        {/* Routes Only visible to users */}

        <Route exact path="/employee_site/my_profile"   render={ props =>  
                                                           isLoggedIn
                                                            ? <MyProfile {...props} userInfo={userInfo} isLoggedIn={isLoggedIn}
                                                            /> 
                                                            : <Redirect to={'/'}/>
                                                            } /> 
                                                          
        <Route exact path="/employee_site/events"      render={ props =>  
                                                           isLoggedIn
                                                            ? <Events {...props} userInfo={userInfo}/> 
                                                            : <Redirect to={'/'}/>
                                                            } /> 
        <Route exact path="/employee_site/employees"      render={ props =>  
                                                           isLoggedIn
                                                            ? <Employees {...props} userInfo={userInfo}/> 
                                                            : <Redirect to={'/'}/>
                                                            } /> 
        <Route exact path="/employee_site/miscellaneous"      render={ props =>  
                                                           isLoggedIn
                                                            ? <Miscellaneous {...props} userInfo={userInfo}/> 
                                                            : <Redirect to={'/'}/>
                                                            } /> 
        <Route exact path="/employee_site/jobtasks"      render={ props =>  
                                                           isLoggedIn
                                                            ? <Jobtasks {...props} userInfo={userInfo}/> 
                                                            : <Redirect to={'/'}/>
                                                            } /> 
        <Route exact path="/employee_site/offices"      render={ props =>  
                                                           isLoggedIn
                                                            ? <Offices {...props} userInfo={userInfo}/> 
                                                            : <Redirect to={'/'}/>
                                                            } /> 

        <Footer/>
      </Router>


  );
}

export default App;
