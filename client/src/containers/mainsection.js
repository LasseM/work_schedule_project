import React from 'react'
import Home from './what_we_offer'
import What_We_Offer from './what_we_offer'
import The_Team from './the_team'
import Our_Locations from './our_locations'
import Our_History from './our_history'
import Testimonials from './testimonials'
import Contact from './contact'
import Work_For_Us from './work_for_us'



import { BrowserRouter as Router, Route } from "react-router-dom"


const MainSection = () => {

    return (
        <div className="mainSection">
            <h2>Main Section</h2>
            
            <Route exact path="/"      component={Home} />
            <Route exact path="/what_we_offer"      component={What_We_Offer} />
            <Route exact path="/the_team" component={The_Team} />
            <Route exact path="/our_locations" component={Our_Locations} />
            <Route exact path="/our_history" component={Our_History} />
            <Route exact path="/testimonials" component={Testimonials} />
            <Route exact path="/contact" component={Contact} />
            <Route exact path="/work_for_us" component={Work_For_Us} />

            

            {/* <Route exact path="/mainsection.js/:data" component={Data}/> */}
        </div>
    )
}

export default MainSection