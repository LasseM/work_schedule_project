
import React from 'react'
// import { NavLink } from 'react-router-dom';

// var secret = 'this-is-the-welcome-secret'
// let url ='/data/${secret}';


const Who_Are_We = () => {


    return (
        <div className="clientPages">   
            <h1>Who Are We</h1>
            <p>DMC Company is a full-service destination management company, with local staff throughout Scandinavia and the Baltics.</p>
            <p>We customize the best possible experiences at our destinations for our clients, always delivered with a personal touch, passion and dedication.</p>
            <p>We have 35+ years of experience as an incoming partner for cruise & MICE operators with a continuous strive to meet or exceed our clients’ expectations.</p>
        
            <h1>What’s in it for our clients?</h1>
            <p>Our clients, however different their budgets may be, always value a reliable partner that’s flexible, trustworthy and
            thorough. They are on the hunt for unique and creative ways to experience our destinations.</p>
            <p>Enter DMC Company.</p>
            <p>35+ years of outstanding destination management have thought us that we can only amaze our guests by always pushing the envelope of what’s expected. We pride ourselves with flawless ground service logistics combined with great local knowledge.</p>
            <p>Others do what we do, we do it differently.</p>
            <p>We are vibrant. We get excited every time. We keep our eyes and mouths open for “the next big thing” to experience. We don’t just stop at meeting our clients’ expectations… because, really, what’s exceptional about that?</p>
            <p>We’re set apart by always wanting to exceed these expectations by striving for cool surprises. You know, the ones that really make an imprint...</p>
            <p>We always want to wow ourselves, because we know at this is how we’d wow you</p>
            <p>We spark innovative thinking in each-other and stay creative in our approach to our work. We may get a little bit crazy, but we’d always guarantee a personal touch, that makes the experience different and will remain memorable long after our clients’ stay in the Nordics.</p>
            <p>What you see, and sometimes what you don’t see, is what you get. Wanna see?</p>
        </div>
    )
}


export default Who_Are_We