import React from "react"
import { NavLink } from 'react-router-dom';
import img1 from '../resources/images/beach_hut_waves_island.jpg'






const Home = () => { 



return (
    <div className="home">   
  
        {/* <NavLink to={url}>Go</NavLink> */}

        <div className="homeSections  img-to-the-left">
            
           
            <div className='homeImg homeSection1'></div>
            <div>
                <h2>Who Are We</h2>
                <p>DMC Company is a full-service destination management company, with local staff throughout Scandinavia and the Baltics.</p>
                <p>We customize the best possible experiences at our destinations for our clients, always delivered with a personal touch, passion and dedication.</p>
                <p>We have 35+ years of experience as an incoming partner for cruise & MICE operators with a continuous strive to meet or exceed our clients’ expectations.</p>
            </div>
        </div>

        <div className="homeSections img-to-the-right">
            <div>
                <h2>Meet The Team</h2>
                <p>DMC Company is a full-service destination management company, with local staff throughout Scandinavia and the Baltics.</p>
                <p>We customize the best possible experiences at our destinations for our clients, always delivered with a personal touch, passion and dedication.</p>
                <p>We have 35+ years of experience as an incoming partner for cruise & MICE operators with a continuous strive to meet or exceed our clients’ expectations.</p>
            </div>
            <div className='homeImg homeSection2'></div>
        </div>

        <div className="homeSections img-to-the-left">
            <div className='homeImg homeSection3'></div>
            <div>
                <h2>Our Locations</h2>
                <p>DMC Company is a full-service destination management company, with local staff throughout Scandinavia and the Baltics.</p>
                <p>We customize the best possible experiences at our destinations for our clients, always delivered with a personal touch, passion and dedication.</p>
                <p>We have 35+ years of experience as an incoming partner for cruise & MICE operators with a continuous strive to meet or exceed our clients’ expectations.</p>
            </div>
        </div>

        <div className="homeSections img-to-the-right">
            <div>
                <h2>Testimonials</h2>
                <p>DMC Company is a full-service destination management company, with local staff throughout Scandinavia and the Baltics.</p>
                <p>We customize the best possible experiences at our destinations for our clients, always delivered with a personal touch, passion and dedication.</p>
                <p>We have 35+ years of experience as an incoming partner for cruise & MICE operators with a continuous strive to meet or exceed our clients’ expectations.</p>
            </div>
            <div className='homeImg homeSection4'></div>
        </div>

        <div className="homeSections img-to-the-left">
            {/* <img alt='img' src={img1}></img> */}
            <div className='homeImg homeSection5'></div>
            <div>
                <h2>Contact</h2>
                <p>DMC Company is a full-service destination management company, with local staff throughout Scandinavia and the Baltics.</p>
                <p>We customize the best possible experiences at our destinations for our clients, always delivered with a personal touch, passion and dedication.</p>
                <p>We have 35+ years of experience as an incoming partner for cruise & MICE operators with a continuous strive to meet or exceed our clients’ expectations.</p>
            </div>
        </div>

        <div className="homeSections img-to-the-right">
            <div>
                <h2>Employee Login</h2>
                <p>DMC Company is a full-service destination management company, with local staff throughout Scandinavia and the Baltics.</p>
                <p>We customize the best possible experiences at our destinations for our clients, always delivered with a personal touch, passion and dedication.</p>
                <p>We have 35+ years of experience as an incoming partner for cruise & MICE operators with a continuous strive to meet or exceed our clients’ expectations.</p>
            </div>
            <div className='homeImg homeSection6'></div>
        </div>
     
    </div>
)}

export default Home