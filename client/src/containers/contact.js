import React, { useState } from 'react'
import GetOffice from '../components/getOffice'
import axios from 'axios'
import { url } from '../config'

const Contact = () =>{

    const [contactForm, setContactForm] = useState({})

    // Props from getOffice component
    const [chosenOffice, setChosenOffice] = useState('')

    const officePicked = (res) => {
        // console.log("res from input =>", res)
        setChosenOffice(res)
        console.log('chosenOffice =>', chosenOffice)
    }

    const handlecontactFormChange = (e) => {
        console.log("contactFormchange", e.target.name, e.target.value)
        setContactForm({...contactForm,[e.target.name]:e.target.value})
        
    }
    console.log('contactForm =>', contactForm)

    const handleSumitForm = async (e) => {
        e.preventDefault()

        const data = {
            name: contactForm.name,
            email: contactForm.email,
            subject: contactForm.subject,
            message: contactForm.message,
        }

        if(contactForm.name === undefined || contactForm.email === undefined || contactForm.subject === undefined || contactForm.message === undefined) return alert(`Please fill in all fields`)
        console.log('form before try', contactForm)
    
    try {
        const response = await axios.post(`${url}emails/send_email`, data)
        console.log('response after try', response)

        if(response.data.ok === true) {
                
        return alert(`Thank you, ${contactForm.name}. Your email with subject: ${contactForm.subject} has been sent to.`)
        }

        }catch(error){
        console.log(error)
    
    }
    }
    return(
        <div className="clientPages contact">

            <h2>Contact us</h2>

            <GetOffice officePicked={officePicked} hideRender={true}/>
            <form onSubmit={handleSumitForm} className='CRUD-form '>
                
                <input 
                name="name"
                type="string"
                placeholder="Type your name here"
                onChange={handlecontactFormChange}
                />
                <input 
                name="email"
                type="email"
                placeholder="Type your email here"
                onChange={handlecontactFormChange}
                />
                <input 
                name="subject"
                type="string"
                placeholder="Type subject title here"
                onChange={handlecontactFormChange}
                />
                <input 
                className="messageFromContactform"
                name="message"
                type="string"
                placeholder="Type your message here"
                onChange={handlecontactFormChange}
                minLength="10"
                maxLength="500"
                required={true}
                />
            </form>

            <h2>Or call our offices directly on </h2>

            {   
            (chosenOffice==='Denmark') ?
                <div>
                    <p>Our Copenhagen Office</p>
                    <p>address</p>
                    <p>Postal Code</p>
                    <p>City</p>
                    <p>Phone</p>
                </div>
             :
                (chosenOffice==='Sweden') ?
                <div>This is the SE Div</div>
                
                :
                 (chosenOffice==='Norway') ?
                 <div>This is the NO Div</div>
                 :
                  (chosenOffice==='Estonia')?
                  <div>This is the EE Div</div>
                  
                    :
                    <div>
                        <h3>This is the All Div</h3>
                        <div>This is the DK Div</div>
                        <div>This is the SE Div</div>
                        <div>This is the NO Div</div>
                        <div>This is the EE Div</div>
                    </div>
                 
            }
        </div>
    )
}

export default Contact