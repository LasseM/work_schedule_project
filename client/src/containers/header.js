import React from 'react'
import { Helmet } from 'react-helmet'
import logo from '../resources/images/dmcLogo.png'


const Header = ()=>{


    return (


        <div className='Header'>
        <Helmet>
            <title>DMC Company</title>
            <meta charSet="utf-8" />
            <meta name="description" content="Website of DMC Company" />
            <meta name='keywords' content='DMC, Destination Management Company, Nordic, Denmark, Sweden, Norway, Estonia'/>
            <meta name='viewport' content='width=device-width, initial-scale=1'/>

                        
            <meta property="og:url" content="https:///" />
            <meta property="og:title" content="DMC Company" />
            <meta property="og:description" content="DMC Company in Denamrk, Sweden, Norway and Estonia" />
            <meta property="og:image" content="" />
            <meta property="og:image:url" content="" />
            <meta name= "twitter:card" content= "summary_large_image"/>
        </Helmet>
            <div className='headerImg'><img src={logo} alt='logo'></img></div>
         
        </div>
    )

}


export default Header