import React from 'react'
import { NavLink } from 'react-router-dom';

var secret = 'this-is-the-welcome-secret'
let url ='/data/${secret}';


const Welcome =() =>{


    return (
        <div className="clientPages">   
            <h1>Welcome</h1>
            <h2>
            <NavLink to={url}>Read More</NavLink>
            </h2>
        </div>
    )
}


export default Welcome