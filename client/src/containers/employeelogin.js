import React, { useState } from 'react';
import axios from 'axios'
import { url } from '../config'



const EmployeeLogin = (props) => {

    console.log("props from Emplogin =>", props)

    
	const [ form , setForm ] = useState({
		employee_email    : '',
		employee_password : ''
	})
	// const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setForm({...form,[e.target.name]:e.target.value})
	}
	const handleSubmit = async (e) => {
        e.preventDefault()
       
		try{
          const response = await axios.post(`${url}/employees/login`, {
            employee_email: form.employee_email,
            employee_password: form.employee_password
          })
          alert(response.data.message)
           console.log("response.data =>", response.data)
        // console.log(message)
          if( response.data.ok ){

              setTimeout( ()=> {
				  props.login(response.data.token, response.data.user)
                  props.history.push('/employee_site/my_profile')
               
			  },2000)    
          }
          
		}
        catch(error){

            console.log(error)
        }
	}
	
    return (
        <div className="employeeloginSection CRUD-form">
            <h2>Log-in for DMC Employees</h2>
            <h3>Sign in here</h3>

            <div className="LoginForms">

            {/* Fulltime Employess */}
            <div className="loginForm">
                    <form onSubmit={handleSubmit}>
         
                        User Email:
                        <input  onChange={handleChange}
                                type='email'
                                name='employee_email'
                        />

                        Password:
                        <input  onChange={handleChange}
                                type="password"
                                name='employee_password'
                        
                        />

                        <button>Log-in</button>
                    </form>

                    <form>
                        <button>Forgot Password</button>
                        <button>Request a Log-in</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default EmployeeLogin
