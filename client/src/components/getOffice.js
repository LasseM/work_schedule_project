import React, { useState, useEffect } from 'react'

const GetOffice = (props) => {

    // console.log('props =>', props)
    let [office, setOffice] = useState({value: "All Our Offices"})



    const handleofficeChange = (e) =>{
        console.log("handleofficeChange", e.target.name, e.target.value)

        setOffice({value: e.target.value})
        console.log('office=>', office.value)

        // props.officePicked(office.value)
    }

      // ====== What to render ======
    const [renderAll, setRenderAll] = useState(true)
    useEffect( () => {
    props.hideRender
    ? setRenderAll(false)
    : setRenderAll(true)
  }, [])

    
    return (
        <div className="getOfficeComponent">
          
            <h3>Choose office</h3>
            
                <div className="officePicker">
                    <button onClick={handleofficeChange}
                    name="officePick"
                    value="All Our Offices"
                    >
                    - All Our Offices -
                    </button>
                    <button onClick={handleofficeChange}
                    name="officePick"
                    value="Denmark"
                    >
                    Denmark
                    </button>
                    <button onClick={handleofficeChange}
                    name="officePick"
                    value="Sweden"
                    >
                    Sweden
                    </button>
                    <button onClick={handleofficeChange}
                    name="officePick"
                    value="Norway"
                    >
                    Norway
                    </button>
                    <button onClick={handleofficeChange}
                    name="officePick"
                    value="Estonia"
                    >
                    Estonia
                    </button>
                </div>

        {   
            renderAll
            ?
            (office.value==='Denmark') ?
             <div>This is the DK Div</div>
             :
                (office.value==='Sweden') ?
                <div>This is the SE Div</div>
                
                :
                 (office.value==='Norway') ?
                 <div>This is the NO Div</div>
                 :
                  (office.value==='Estonia')?
                  <div>This is the EE Div</div>
                  
                    :
                    <div>
                        <h3>This is the All Div</h3>
                        <div>This is the DK Div</div>
                        <div>This is the SE Div</div>
                        <div>This is the NO Div</div>
                        <div>This is the EE Div</div>
                    </div>
                 
            : null
            }
        </div>
    )
}

export default GetOffice