import React from 'react'
import widgetStyle from './widgetStyle';
// import axios from 'axios'
// import {url} from './../../../config'

export default class UploadImages extends React.Component{

	state={}

	uploadWidget = () => {
        window.cloudinary.openUploadWidget({ 
        	cloud_name: 'dkbo4lvxt', 
        	upload_preset: 'ayt04f0j', 
			tags:['user'],
			stylesheet:widgetStyle
        },
            async (error, result)=> {
				debugger
                if(error){
					debugger
                }else{
					this.props.getImages({
						photo_url:result[0].secure_url, 
						public_id:result[0].public_id
						})
							  
                }
            });
    }

	render(){
		return (
			<div className="flex_upload">
                <div className="upload">
					<button className ="button_small"
                    	onClick={this.uploadWidget} > click me
                    </button>
                </div>
            </div>
		)
	}
}
