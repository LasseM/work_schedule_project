import React from "react";
import { NavLink } from "react-router-dom";


const Navbar = () => {
  return (
    <div className='NavigationBar'>
      <NavLink
      exact
        to={"/Who_Are_We"}
      >
        Who We Are
      </NavLink>

      <NavLink
        exact
        to={"/the_team"}
      >
        Meet The Team
      </NavLink>

      <NavLink
        exact
        to={"/our_locations"}
      >
        Our Locations
      </NavLink>

      {/* <NavLink
        exact
        to={"/our_history"}
      >
        Our History
      </NavLink> */}

      <NavLink
        exact
        to={"/testimonials"}
      >
        Testimonials
      </NavLink>

      <NavLink
        exact
        to={"/contact"}
      >
        Contact
      </NavLink>

      <NavLink
        exact
        to={"/employee_login"}
      >
        Employee Login
      </NavLink>
    </div>
  );
};


export default Navbar