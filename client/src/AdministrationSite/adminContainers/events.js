import React, { useState } from 'react'

import { BrowserRouter as Router, Route, Redirect } from "react-router-dom"

import { NavLink } from "react-router-dom";


import EventFind from '../adminComponents/events/events_find'
import EventFindOne from '../adminComponents/events/find_one_event'
import EventAdd from '../adminComponents/events/event_add'
import EventUpdate from '../adminComponents/events/event_update'
import EventRemove from '../adminComponents/events/event_remove'


const Events = (props) => {

    const [filter, setFilter] = useState(false)
    const handleFilterShow = () => {
        setFilter(true)
    }
console.log('props =>', props.userInfo)
    return (
        <Router>
        <div className='eventSection'>

        <p>Hello here you can find, create, update or remove events</p>
   

        {   props.userInfo.employee_type === 'Freelancer'
    ?
    <div className="navlinks">
    {/* ====== Search for events ====== */}
            <NavLink
            exact
            to={"/employee_site/events/event_find"}
            onClick={handleFilterShow}
            >
            <div>Search for all events</div>
            </NavLink>
    {/* ====== Search for One event ====== */}
            <NavLink
            exact
            to={"/employee_site/events/find_one_event/"}
            >
            <div>Search for one specific event</div>
            </NavLink>

        </div>
    :    <div className="navlinks">
    
    {/* ====== Search for events ====== */}
            <NavLink
            exact
            to={"/employee_site/events/event_find"}
            onClick={handleFilterShow}
            >
            <div>Search for all events</div>
            </NavLink>
    {/* ====== Search for One event ====== */}
            <NavLink
            exact
            to={"/employee_site/events/find_one_event/"}
            >
            <div>Search for one specific event</div>
            </NavLink>
    {/* ====== Add a new event ====== */}
            <NavLink
            exact
            to={"/employee_site/events/event_add"}
            props={props}
            >
            <div>Add a new event</div>
            </NavLink>
    {/* ====== Update one event ====== */}
            <NavLink
                exact
                to={"/employee_site/events/event_update"}
                >
                <div>Update one event</div>
                </NavLink>

    {/* ====== Remove one event ====== */}
            <NavLink
                exact
                to={"/employee_site/events/event_remove"}
                >
                <div>Remove one event</div>
                </NavLink>
        </div>           
    }
    {filter ? <div className="filter"> <div>Show filters </div>   </div> : <span></span>}

    <Route exact path="/employee_site/events/event_find"      component={EventFind} />
    <Route exact path="/employee_site/events/find_one_event"      component={EventFindOne} />
    <Route exact path="/employee_site/events/event_add"      component={EventAdd} />
    <Route exact path="/employee_site/events/event_update"      component={EventUpdate} />

    {/* <Route exact path="/employee_site/events/event_update"      render={ props =>  
                                                           props.isLoggedIn
                                                            ? <EventUpdate {...props} 
                                                            /> 
                                                            : <Redirect to={'/'}/>
                                                            } />  */}
                                                            
    <Route exact path="/employee_site/events/event_remove"      component={EventRemove} />

    
    </div>       
        </Router>

    )
}

export default Events