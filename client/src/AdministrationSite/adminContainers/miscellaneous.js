import React from 'react'
import DropDownEmployees from '../adminComponents/employees/dropDownEmployees'

const Miscellaneous = () => {

    return (
        <div className='miscellaneousSection CRUD-form'>Here you can find Miscellaneous Stuff

        <DropDownEmployees/>
        </div>
    )
}

export default Miscellaneous