import React, { useState } from 'react'

import { BrowserRouter as Router, Route } from "react-router-dom"

import { NavLink } from "react-router-dom";


import JobtaskFind from '../adminComponents/jobtasks/jobtask_find'
import JobtaskFindOne from '../adminComponents/jobtasks/find_one_jobtask'
import JobtaskAdd from '../adminComponents/jobtasks/jobtask_add'
import JobtaskUpdate from '../adminComponents/jobtasks/jobtask_update'
import JobtaskRemove from '../adminComponents/jobtasks/jobtask_remove'


const Jobtasks = () => {

    const [filter, setFilter] = useState(false)
    const handleFilterShow = () => {
        setFilter(true)
    }

    return (
        <Router>
        <div className='jobtaskSection CRUD-form'>

        <p>Hello here you can find, create, update or remove jobtasks</p>

        <div className="navlinks">
    {/* ====== Search for jobtasks ====== */}
            <NavLink
            exact
            to={"/employee_site/jobtasks/jobtask_find"}
            onClick={handleFilterShow}
            >
            <div>Search for all jobtasks</div>
            </NavLink>
    {/* ====== Search for One jobtask ====== */}
            <NavLink
            exact
            to={"/employee_site/jobtasks/find_one_jobtask/"}
            >
            <div>Search for one specific jobtask</div>
            </NavLink>
    {/* ====== Add a new jobtask ====== */}
            <NavLink
            exact
            to={"/employee_site/jobtasks/jobtask_add"}
            >
            <div>Add a new jobtask</div>
            </NavLink>
    {/* ====== Update one jobtask ====== */}
            <NavLink
                exact
                to={"/employee_site/jobtasks/jobtask_update"}
                >
                <div>Update one jobtask</div>
                </NavLink>

    {/* ====== Remove one jobtask ====== */}
            <NavLink
                exact
                to={"/employee_site/jobtasks/jobtask_remove"}
                >
                <div>Remove one jobtask</div>
                </NavLink>
        </div>           

    {filter ? <div className="filter"> <div>Show filters </div>   </div> : <span></span>}

    <Route exact path="/employee_site/jobtasks/jobtask_find"      component={JobtaskFind} />
    <Route exact path="/employee_site/jobtasks/find_one_jobtask"      component={JobtaskFindOne} />
    <Route exact path="/employee_site/jobtasks/jobtask_add"      component={JobtaskAdd} />
    <Route exact path="/employee_site/jobtasks/jobtask_update"      component={JobtaskUpdate} />
    <Route exact path="/employee_site/jobtasks/jobtask_remove"      component={JobtaskRemove} />

    
    </div>       
        </Router>

    )
}

export default Jobtasks