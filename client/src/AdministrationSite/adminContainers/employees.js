import React, { useState } from 'react'

import { BrowserRouter as Router, Route } from "react-router-dom"

import { NavLink } from "react-router-dom";


import EmployeeFind from '../adminComponents/employees/employees_find'
import EmployeeFindOne from '../adminComponents/employees/find_one_employee'
import EmployeeRegister from '../adminComponents/employees/employee_register'
import EmployeeUpdate from '../adminComponents/employees/employee_update'
import EmployeeRemove from '../adminComponents/employees/employee_remove'

const Employees = () => {

    const [filter, setFilter] = useState(false)
    const handleFilterShow = () => {
        setFilter(true)
    }
    return (
        <Router>
        <div className='employeeSection'>

        <p>Here you can find, create, update or remove Employees</p>

        <div className="navlinks">
    {/* ====== Search for Employees ====== */}
            <NavLink
            exact
            to={"/employee_site/employees/employee_find"}
            onClick={handleFilterShow}
            >
            <div>Search for all Employees</div>
            </NavLink>
    {/* ====== Search for One Employee ====== */}
            <NavLink
            exact
            to={"/employee_site/employees/find_one_employee/"}
            >
            <div>Search for one specific Employee</div>
            </NavLink>
    {/* ====== Add a new Employee ====== */}
            <NavLink
            exact
            to={"/employee_site/employees/employee_register"}
            >
            <div>Add a new Employee</div>
            </NavLink>
    {/* ====== Update one Employee ====== */}
            <NavLink
                exact
                to={"/employee_site/employees/employee_update"}
                >
                <div>Update one Employee</div>
                </NavLink>

    {/* ====== Remove one Employee ====== */}
            <NavLink
                exact
                to={"/employee_site/employees/employee_remove"}
                >
                <div>Remove one Employee</div>
                </NavLink>
        </div>           


        {filter ? <div className="filter"> <div>Show filters </div>   </div> : <span></span>}

    <Route exact path="/employee_site/employees/employee_find"      component={EmployeeFind} />
    <Route exact path="/employee_site/employees/find_one_employee"      component={EmployeeFindOne} />
    <Route exact path="/employee_site/employees/employee_register"      component={EmployeeRegister} />
    <Route exact path="/employee_site/employees/employee_update"      component={EmployeeUpdate} />
    <Route exact path="/employee_site/employees/employee_remove"      component={EmployeeRemove} />
    
    </div>       
        </Router>

    )
}

export default Employees