import React, { useState, useEffect } from 'react'
// import EmptyFormEmployeeContent from '../components/employees/emptyFormEmployeeContent'
import { url } from '../../config'
import axios from 'axios'
// import EmployeeFindOne from '../components/employees/find_one_employee'
import EmployeeUpdate from '../adminComponents/employees/employee_update'

const MyProfile = (props) => {
    
    const [MyProfileForm, setMyProfileForm] = useState({})
    
    // const [input, setInput] = useState({})
    
      console.log("props.userInfo", props.userInfo)
    
    useEffect( () => {
        setMyProfileForm({...MyProfileForm, ...props.userInfo})
       
        },[])
        
      console.log("MyProfileForm =>", MyProfileForm)
    const handleFormChange = (e) => {
        setMyProfileForm({...MyProfileForm,[e.target.name]:e.target.value})
    }

    // const CancelChange = () => {
    //     setForm({...form, ...props.userInfo})
    //   }
    
    // const handleSubmit = async (e) => {
    //     e.preventDefault()

    //     const data = {
    //         _id: form._id,
    //         employeeID: form.employeeID,
    //         employee_password: form.employee_password,
    //         employee_password2: form.employee_password2,
    //         employee_type: form.employee_type,
    //         employee_image: form.employee_image,
    //         employee_firstname: form.employee_firstname,
    //         employee_middlename: form.employee_middlename,
    //         employee_lastname: form.employee_lastname,
    //         employee_email: form.employee_email,
    //         employee_addressStreet: form.employee_addressStreet,
    //         employee_addressNumber: form.employee_addressNumber,
    //         employee_postalCode: form.employee_postalCode,
    //         employee_city: form.employee_city,
    //         employee_country: form.employee_country,
    //         employee_phone: form.employee_phone,
    //         employee_mobile: form.employee_mobile,
    //         employee_office: form.employee_office,
    //         employee_position: form.employee_position,
    //         employee_focusarea: form.employee_focusarea,
    //         employee_nationality: form.employee_nationality,
    //         employee_language: form.employee_language,
    //         employee_bank_reg: form.employee_bank_reg,
    //         employee_bank_account_number: form.employee_bank_account_number,
    //         employee_payroll_num: form.employee_payroll_num,
    //         employee_tax_card: form.employee_tax_card,
    //     }
        
    //     console.log('data =>', data)

    //     if(form.employee_type === '' || form.employee_country_location === '') return alert(`Please fill in all fields`)
    //     console.log('form before try', form)
        
    //     try {
    //         const response = await axios.put(`${url}/employees/update_employee`, data)
    //         console.log('response =>', response)
    //         alert(`Employee with id: >> ${form._id} << and name: ${form.employee_firstname} ${form.employee_lastname} has been updated`)
    //     }catch(error){
    //         console.log(error)
    //         alert(error.data.message)
    //     }
    // }
    // debugger
    return (
        <div className="my_profileSection">

            <h1>Here you can see your profile info:</h1>

        <EmployeeUpdate hideheadingsMyProfile MyProfileForm={MyProfileForm}  />
        {/* input={input} */}
                    {/* <EmployeeFindOne input={input}/> */}
             {/* <form onSubmit={handleSubmit} className="CRUD-form">
                <h3>Update the employee below:</h3>
        
                <div>* = required fields</div>


                employee_firstname: <input 
                                    required
                                    name='employee_firstname'
                                    onChange={handleFormChange}
                                    value={form.employee_firstname}
                                    placeholder=''/>
                employeeID:         <input
                                    name='employeeID'
                                    onChange={handleFormChange}
                                    value={form.employeeID}
                                    placeholder=''
                                    readOnly="true"
                                    />
                *employee_password: <input
                                    required
                                    name='employee_password'
                                    onChange={handleFormChange}
                                    value={form.employee_password}
                                    placeholder=''/>
              
                    *employee_type:  <input 
                                    name="form.employee_type" 
                                    value={form.employee_type}
                                    readOnly="true"
                                    />

                    employee_image: <img 
                                        //src={props.userInfo.employee_image.photo_url}
                                        className="profileImg"
                                        name='employee_image'
                                        onChange={handleFormChange}
                                        alt="profile img"
                                        placeholder=''/>
                    employee_middlename: <input name='employee_middlename'
                                        onChange={handleFormChange}
                                        value={form.employee_middlename}
                                        placeholder=''/>
                    *employee_lastname: <input 
                                        required
                                        name='employee_lastname'
                                        onChange={handleFormChange}
                                        value={form.employee_lastname}
                                        placeholder=''/>
                    employee_email: <input name='employee_email'
                                        onChange={handleFormChange}
                                        value={form.employee_email}
                                        placeholder=''/>
                    employee_addressStreet: <input name='employee_addressStreet'
                                        onChange={handleFormChange}
                                        value={form.employee_addressStreet}
                                        placeholder=''/>
                    employee_addressNumber: <input name='employee_addressNumber'
                                        onChange={handleFormChange}
                                        value={form.employee_addressNumber}
                                        placeholder=''/>
                    employee_postalCode: <input name='employee_postalCode'
                                        onChange={handleFormChange}
                                        value={form.employee_postalCode}
                                        placeholder=''/>
                    employee_city: <input name='employee_city'
                                        onChange={handleFormChange}
                                        value={form.employee_city}
                                        placeholder=''/>
                    employee_country: <input name='employee_country'
                                        readOnly="true"
                                        value={form.employee_country}
                                        />
                    employee_phone: <input name='employee_phone'
                                        onChange={handleFormChange}
                                        value={form.employee_phone}
                                        placeholder=''/>
                    employee_mobile: <input name='employee_mobile'
                                        onChange={handleFormChange}
                                        value={form.employee_mobile}
                                        placeholder=''
                                        readOnly="true"/>
                    employee_office: <input name='employee_office'
                                        readOnly="true"
                                        value={form.employee_office}
                                        />
                    employee_position: <input name='employee_position'
                                        readOnly="true"
                                        value={form.employee_position}
                                        />
                    employee_focusarea: <input name='employee_focusarea'
                                        onChange={handleFormChange}
                                        value={form.employee_focusarea}
                                        placeholder=''/>
                    employee_nationality: <input name='employee_nationality'
                                        onChange={handleFormChange}
                                        value={form.employee_nationality}
                                        placeholder=''/>
                    employee_language: <input name='employee_language'
                                        onChange={handleFormChange}
                                        value={form.employee_language}
                                        placeholder=''/>
                    employee_bank_reg: <input name='employee_bank_reg'
                                        onChange={handleFormChange}
                                        value={form.employee_bank_reg}
                                        placeholder=''/>
                    employee_bank_account_number: <input name='employee_bank_account_number'
                                        onChange={handleFormChange}
                                        value={form.employee_bank_account_number}
                                        placeholder=''/>
                    employee_payroll_num: <input name='employee_payroll_num'
                                        onChange={handleFormChange}
                                        value={form.employee_payroll_num}
                                        placeholder=''/>
                    employee_tax_card: <input name='employee_tax_card'
                                        onChange={handleFormChange}
                                        value={form.employee_tax_card}
                                        placeholder=''/>
                    <button>Update the Employee</button>
                </form> */} 

                <div className="buttons">
                {/* <button className="cancelChange" onClick={CancelChange}>Cancel Changes</button> */}
                    <button>Change Password</button>
                </div>
            
        </div>

    )
}
export default MyProfile