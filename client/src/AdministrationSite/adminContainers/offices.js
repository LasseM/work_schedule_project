import React, { useState } from 'react'

import { BrowserRouter as Router, Route } from "react-router-dom"

import { NavLink } from "react-router-dom";


import OfficeFind from '../adminComponents/offices/office_find'
import OfficeFindOne from '../adminComponents/offices/find_one_office'
import OfficeAdd from '../adminComponents/offices/office_add'
import OfficeUpdate from '../adminComponents/offices/office_update'
import OfficeRemove from '../adminComponents/offices/office_remove'


const Offices = () => {

    const [filter, setFilter] = useState(false)
    const handleFilterShow = () => {
        setFilter(true)
    }

    return (
        <Router>
        <div className='officeSection CRUD-form'>

        <p>Hello here you can find, create, update or remove offices</p>

        <div className="navlinks">
    {/* ====== Search for offices ====== */}
            <NavLink
            exact
            to={"/employee_site/offices/office_find"}
            onClick={handleFilterShow}
            >
            <div>Search for all offices</div>
            </NavLink>
    {/* ====== Search for One office ====== */}
            <NavLink
            exact
            to={"/employee_site/offices/find_one_office/"}
            >
            <div>Search for one specific office</div>
            </NavLink>
    {/* ====== Add a new office ====== */}
            <NavLink
            exact
            to={"/employee_site/offices/office_add"}
            >
            <div>Add a new office</div>
            </NavLink>
    {/* ====== Update one office ====== */}
            <NavLink
                exact
                to={"/employee_site/offices/office_update"}
                >
                <div>Update one office</div>
                </NavLink>

    {/* ====== Remove one office ====== */}
            <NavLink
                exact
                to={"/employee_site/offices/office_remove"}
                >
                <div>Remove one office</div>
                </NavLink>
        </div>           

    {filter ? <div className="filter"> <div>Show filters </div>   </div> : <span></span>}

    <Route exact path="/employee_site/offices/office_find"      component={OfficeFind} />
    <Route exact path="/employee_site/offices/find_one_office"      component={OfficeFindOne} />
    <Route exact path="/employee_site/offices/office_add"      component={OfficeAdd} />
    <Route exact path="/employee_site/offices/office_update"      component={OfficeUpdate} />
    <Route exact path="/employee_site/offices/office_remove"      component={OfficeRemove} />

    
    </div>       
        </Router>

    )
}

export default Offices