import React, { useState, useEffect }  from "react";

import { NavLink } from "react-router-dom";


//props
const EmployeeNavbar = (props) => {

console.log("props.userInfo Nav", props.userInfo.employee_type)

let [isFreelancer, setIsFreelancer] = useState(false)
let [isAdmin, setIsAdmin] = useState(false)

// const logout = () => {
//   props.logout
// }
useEffect( () => {
  (props.userInfo.employee_type === "Freelancer")
  ? setIsFreelancer(true)
  : setIsFreelancer(false);

  
  (props.userInfo.employee_type === "Admin")
  ? setIsAdmin(true)
  : setIsAdmin(false)

},[])





// ============================== //
// ====== Freelance Navbar ====== //
// ============================== //

if(isFreelancer === true) {
  return (<div className='EmployeeNavbar freelanceNav'>
      
      <NavLink
      exact
        to={"/employee_site/my_profile"}
      >
        My Profile
      </NavLink>


      <NavLink
      exact
        to={"/employee_site/events"}
      >
        Events
      </NavLink>

      {/* <NavLink
        exact
        to={"/employee_site/jobtasks"}
      >
        Jobtasks
      </NavLink> */}

      {/* <NavLink
        exact
        to={"/employee_site/miscellaneous"}
      >
        Miscellaneous
      </NavLink> */}


  <button onClick={()=> props.logout()}>logout</button>
    </div>
  )};
    // ========================== //
    // ====== Admin Navbar ====== //
    // ========================== //
        if(isAdmin === true){
        return (<div className='EmployeeNavbar adminNav'>
          
          <NavLink
          exact
            to={"/employee_site/my_profile"}
          >
            My Profile
          </NavLink>


          <NavLink
          exact
            to={"/employee_site/events"}
            isFreelancer={isFreelancer}
          >
            Events
          </NavLink>


          <NavLink
            exact
            to={"/employee_site/employees"}
          >
            Employees
          </NavLink>

          <NavLink
            exact
            to={"/employee_site/miscellaneous"}
          >
            Miscellaneous
          </NavLink>

          <NavLink
            exact
            to={"/employee_site/jobtasks"}
          >
            Jobtasks
          </NavLink>

          <NavLink
            exact
            to={"/employee_site/offices"}
          >
            Offices
          </NavLink>

          
          <button onClick={()=> props.logout()}>logout</button>
          </div>
          
          )}
    // ============================ //
    // ====== Generel Navbar ====== //
    // ============================ //
          else{
          return(<div className='EmployeeNavbar'>
          
          <NavLink
          exact
            to={"/employee_site/my_profile"}
          >
            My Profile
          </NavLink>


          <NavLink
          exact
            to={"/employee_site/events"}
          >
            Events
          </NavLink>


          <NavLink
            exact
            to={"/employee_site/employees"}
          >
            Employees
          </NavLink>

          {/* <NavLink
            exact
            to={"/employee_site/miscellaneous"}
          >
            Miscellaneous
          </NavLink> */}

          <NavLink
            exact
            to={"/employee_site/jobtasks"}
          >
            Jobtasks
          </NavLink>

          {/* <NavLink
            exact
            to={"/employee_site/offices"}
          >
            Offices
          </NavLink> */}

          <button onClick={()=> props.logout()}>logout</button>
      

    </div>
          )}
  
};


export default EmployeeNavbar