import React, { useState, useEffect } from 'react'
import { url } from '../../../config'
import axios from 'axios'

const OfficeFindOne = (props) => {

  const [form, setForm] = useState('')
  const [findOneOffice, setFindOneOffice] = useState([])
  const [notThere, setNotThere] = useState(false)
  

  
  const handleFormChange = (e) => {
    console.log(e.target.name, e.target.value)
    setForm({...form,[e.target.name]:e.target.value})
}

// ====== Render Toggle ======
const [renderAll, setRenderAll] = useState(true)
  useEffect( () => {
    props.hideRender
    ? setRenderAll(false)
    : setRenderAll(true)
  }, [])


  const handleSubmit = async (e) => {
    e.preventDefault()

    console.log("form.office_firstname =>", form.office_firstname)
    if(form.office_firstname === undefined && form._id === undefined) return alert(`Please fill in either office Name or office System ID before search`);
  
    
    const data = {
      office_firstname: form.office_firstname,
      _id: form._id,
      
    }
    
    try {
      const response = await axios.post(`${url}/offices/find_one_office/`, data)
      console.log("response", response)
      
      

    if(response.data.ok) {
      let current = findOneOffice
      current =[response.data.response]
      setFindOneOffice([...current])
      props.input(response.data.response)
      setNotThere(false) 
    } else {
      setNotThere(true)
      setFindOneOffice([])
     }
    

  }catch(error){
      console.log(error)
  }
  }
      return (
          <div className="CRUD-form">
            <p>Here you can search for one specific offices</p>
            <form onSubmit={handleSubmit} className="onSubmitForm">
            
            
            office Firstname:<input name='office_firstname'
                            onChange={handleFormChange}
                            value={form.office_firstname}
                            placeholder=''/> 
            
            
            office Object_id:<input name='_id'
                            onChange={handleFormChange}
                            value={form._id}
                            placeholder=''/> 
            
            <button>Search</button>
            </form>

        {
          renderAll
          ? findOneOffice.length !== 0
           ? findOneOffice.map( (ele, idx) =>{
                    
                    console.log('ele from UpdateOneOffice:', ele)
                return  <div key={idx} className="returnedOffices">
      <p>office:</p>
        <p className="list">office_firstname:</p> 
        <input 
        className="resultInLists"
        value={ele.office_firstname}
        readOnly="true"
        />
        <p className="list">systemID:</p> 
        <input 
        className="resultInLists"
        value={ele._id}
        readOnly="true"
        />
        <p className="list">officeID:</p> 
        <input 
        className="resultInLists"
        value={ele.officeID}
        readOnly="true"
        /> 
        <p className="list">office_image:</p> 
        <input 
        className="resultInLists"
        value={ele.office_image}
        readOnly="true"
        /> 
        <p className="list">office_logo:</p> 
        <input 
        className="resultInLists"
        value={ele.office_logo}
        readOnly="true"
        /> 
        <p className="list">office_middlename:</p> 
        <input 
        className="resultInLists"
        value={ele.office_middlename}
        readOnly="true"
        /> 
        <p className="list">office_lastname:</p> 
        <input 
        className="resultInLists"
        value={ele.office_lastname}
        readOnly="true"
        /> 
        <p className="list">office_country_location:</p> 
        <input 
        className="resultInLists"
        value={ele.office_country_location}
        readOnly="true"
        /> 
        <p className="list">office_city_location:</p> 
        <input 
        className="resultInLists"
        value={ele.office_city_location}
        readOnly="true"
        /> 
        <p className="list">office_location:</p> 
        <input 
        className="resultInLists"
        value={ele.office_location}
        readOnly="true"
        /> 
        <p className="list">office_adress_street:</p> 
        <input 
        className="resultInLists"
        value={ele.office_adress_street}
        readOnly="true"
        /> 
        <p className="list">office_adress_number:</p> 
        <input 
        className="resultInLists"
        value={ele.office_adress_number}
        readOnly="true"
        /> 
        <p className="list">office_adress_postcode:</p> 
        <input 
        className="resultInLists"
        value={ele.office_adress_postcode}
        readOnly="true"
        /> 
        <p className="list">office_VAT_Num:</p> 
        <input 
        className="resultInLists"
        value={ele.office_VAT_Num}
        readOnly="true"
        /> 
        <p className="list">office_telephone:</p> 
        <input 
        className="resultInLists"
        value={ele.office_telephone}
        readOnly="true"
        /> 
        <p className="list">office_email:</p> 
        <input 
        className="resultInLists"
        value={ele.office_email}
        readOnly="true"
        /> 
        <p className="list">office_SoMe_One:</p> 
        <input 
        className="resultInLists"
        value={ele.office_SoMe_One}
        readOnly="true"
        /> 
        <p className="list">office_SoMe_Two:</p> 
        <input 
        className="resultInLists"
        value={ele.office_SoMe_Two}
        readOnly="true"
        /> 
        <p className="list">office_SoMe_Three:</p> 
        <input 
        className="resultInLists"
        value={ele.office_SoMe_Three}
        readOnly="true"
        /> 
        <p className="list">office_SoMe_Four:</p> 
        <input 
        className="resultInLists"
        value={ele.office_SoMe_Four}
        readOnly="true"
        /> 
        <p className="list">office_SoMe_Five:</p> 
        <input 
        className="resultInLists"
        value={ele.office_SoMe_Five}
        readOnly="true"
        /> 
        <p className="list">office_SoMe_Six:</p> 
        <input 
        className="resultInLists"
        value={ele.office_SoMe_Six}
        readOnly="true"
        /> 
        <p className="list">office_language:</p> 
        <input 
        className="resultInLists"
        value={ele.office_language}
        readOnly="true"
        /> 
                </div>
                  }):<div>Input not recognized</div>
      : null
      }
      

          </div>
      )
}

export default OfficeFindOne