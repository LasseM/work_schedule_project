const EmptyFormOfficeContent = {

    officeID: '',
    office_image: {
        photo_url: "",
        public_id: "",
    }, 
    office_logo: {
        photo_url: "",
        public_id: "",
    },
    office_firstname: '',
    office_middlename: '',
    office_lastname: '',
    office_country_location: '',
    office_city_location: '',
    office_location: '',
    office_adress_street: '',
    office_adress_number: '',
    office_adress_postcode: '',
    office_VAT_Num: '',
    office_telephone: '',
    office_email: '',
    office_SoMe_One: '',
    office_SoMe_Two: '',
    office_SoMe_Three: '',
    office_SoMe_Four: '',
    office_SoMe_Five: '',
    office_SoMe_Six: '',
    office_language: '',  
    

}
export default EmptyFormOfficeContent