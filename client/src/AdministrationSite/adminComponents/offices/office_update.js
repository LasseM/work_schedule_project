import React, { useState } from 'react'
import emptyFormOfficeContent from './emptyFormOfficeContent'
import { url } from '../../../config'
import axios from 'axios'
import OfficeFindOne from './find_one_office'



const OfficeUpdate = () => {
    const [initialInput, setInitialInput] = useState({})
    const [inputsFromSearch, setInputsFromSearch] = useState({})
    const [form, setForm] = useState(emptyFormOfficeContent)

    //Toggle Render
    //const [toggleRender] = useState(false)
    
 
    

    const input = (res) => {
        console.log("res from input =>", res)
        setInputsFromSearch({...inputsFromSearch, ...res})
        setInitialInput({...inputsFromSearch, ...res})

    }
    console.log("inputsFromSearch after input =>", inputsFromSearch)

    const handleFormChange = (e) => {
        console.log(e.target.name, e.target.value)
        setInputsFromSearch({...inputsFromSearch,[e.target.name]:e.target.value})
    }

    const CancelChange = (res) => {
        setInputsFromSearch({...inputsFromSearch, ...initialInput})
      }
    
    const handleSubmit = async (e) => {
        e.preventDefault()

        const data = {
            _id: inputsFromSearch._id,
            officeID: inputsFromSearch.officeID,
            office_image: inputsFromSearch.office_image,
            office_logo: inputsFromSearch.office_logo,
            office_firstname: inputsFromSearch.office_firstname,
            office_middlename: inputsFromSearch.office_middlename,
            office_lastname: inputsFromSearch.office_lastname,
            office_country_location: inputsFromSearch.office_country_location,
            office_city_location: inputsFromSearch.office_city_location,
            office_location: inputsFromSearch.office_location,
            office_adress_street: inputsFromSearch.office_adress_street,
            office_adress_number: inputsFromSearch.office_adress_number,
            office_adress_postcode: inputsFromSearch.office_adress_postcode,
            office_VAT_Num: inputsFromSearch.office_VAT_Num,
            office_telephone: inputsFromSearch.office_telephone,
            office_email: inputsFromSearch.office_email,
            office_SoMe_One: inputsFromSearch.office_SoMe_One,
            office_SoMe_Two: inputsFromSearch.office_SoMe_Two,
            office_SoMe_Three: inputsFromSearch.office_SoMe_Three,
            office_SoMe_Four: inputsFromSearch.office_SoMe_Four,
            office_SoMe_Five: inputsFromSearch.office_SoMe_Five,
            office_SoMe_Six: inputsFromSearch.office_SoMe_Six,
            office_language: inputsFromSearch.office_language,
        }
        
        console.log('data =>', data)

        if(inputsFromSearch.office_firstname === '' || inputsFromSearch.office_country_location === '') return alert(`Please fill in all fields`)
        console.log('form before try', form)
        
        try {
            const response = await axios.put(`${url}/offices/update_office`, data)
            console.log(response)
            alert(`office with id: >> ${inputsFromSearch._id} << and name: ${inputsFromSearch.office_firstname} has been updated`)
        }catch(error){
            console.log(error)
        }
    }
    return (
        <div >
                    <OfficeFindOne input={input} hideRender={true}/>
            <form onSubmit={handleSubmit} className="CRUD-form">
                <h3>Update the office below:</h3>
        
                <div>* = required fields</div>

                *<p>Office Name:</p> 
                                    <input 
                                    required
                                    name='office_firstname'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.office_firstname}
                                    placeholder=''/>
                <p>Office ID:</p>
                                    <input name='officeID'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.officeID}
                                        placeholder=''/> 
                <p>Office Image:</p>
                                     <input name='office_image'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_image}
                                        placeholder=''/>
                <p>Office Logo:</p>
                                     <input name='office_logo'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_logo}
                                        placeholder=''/>
                <p>Office Subname:</p>
                                     <input name='office_middlename'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_middlename}
                                        placeholder=''/>
                <p>Office Lastname:</p>
                                     <input name='office_lastname'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_lastname}
                                        placeholder=''/>
                <p>Office Country Location:</p>
                                     <select required onChange={handleFormChange} name='office_country_location'>
                                        <option value="default">- choose country office city location -</option>
                                        <option value="Denmark">Denmark</option>
                                        <option value="Sweden">Sweden</option>
                                        <option value="Norway">Norway</option>
                                        <option value="Estonia">Estonia</option>
                                    </select>
                <p>Office City Location:</p>
                                     <input 
                                        required
                                        name='office_city_location'
                                        onChange={handleFormChange}
                                    value={inputsFromSearch.office_city_location}
                                    placeholder=''/>
                <p>Office Location:</p>
                                     <input name='office_location'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_location}
                                        placeholder=''/>
                <p>Office Adress Street:</p>
                                     <input name='office_adress_street'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_adress_street}
                                        placeholder=''/>
                <p>Office Adress Number:</p>
                                     <input name='office_adress_number'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_adress_number}
                                        placeholder=''/>
                <p>Office Adress Postcode:</p>
                                     <input name='office_adress_postcode'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_adress_postcode}
                                        placeholder=''/>
                <p>Office Adress Country:</p>
                                     <input name='office_VAT_Num'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_VAT_Num}
                                        placeholder=''/>
                <p>Office Telephone:</p>
                                     <input name='office_telephone'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_telephone}
                                        placeholder=''/>
                <p>Office Email:</p>
                                     <input name='office_email'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.even_email}
                                        placeholder=''/>
                <p>Office SoMe_One:</p>
                                     <input name='office_SoMe_One'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_SoMe_One}
                                        placeholder=''/>
                <p>Office SoMe_Two:</p>
                                     <input name='office_SoMe_Two'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_SoMe_Two}
                                        placeholder=''/>
                <p>Office SoMe_Three:</p>
                                     <input name='office_SoMe_Three'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_SoMe_Three}
                                        placeholder=''/>
                <p>Office SoMe_Four:</p>
                                     <input name='office_SoMe_Four'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_SoMe_Four}
                                        placeholder=''/>
                <p>Office SoMe_Five:</p>
                                     <input name='office_SoMe_Five'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_SoMe_Five}
                                        placeholder=''/>
                <p>Office SoMe_Six:</p>
                                     <input name='office_SoMe_Six'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_SoMe_Six}
                                        placeholder=''/>
                <p>Office Language:</p>
                                     <input name='office_language'
                                        onChange={handleFormChange}
                                        value={inputsFromSearch.office_language}
                                        placeholder=''/>
            <button>Update the office</button>
            </form>

            <button className="cancelChange" onClick={CancelChange}>Cancel Changes</button>
        </div>

    )
}


export default OfficeUpdate