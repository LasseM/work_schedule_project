import React, { useState } from 'react'
import formOfficeContent from './formOfficeContent'
import { url } from '../../../config'
import axios from 'axios'

const OfficeAdd = () => {
    
    const [form, setForm] = useState(formOfficeContent)
    
    const handleFormChange = (e) => {
        console.log(e.target.name, e.target.value)
        setForm({...form,[e.target.name]:e.target.value})
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        
        const data = {
        officeID: form.officeID,
        office_image: form.office_image,
        office_logo: form.office_logo,
        office_firstname: form.office_firstname,
        office_middlename: form.office_middlename,
        office_lastname: form.office_lastname,
        office_country_location: form.office_country_location,
        office_city_location: form.office_city_location,
        office_location: form.office_location,
        office_adress_street: form.office_adress_street,
        office_adress_number: form.office_adress_number,
        office_adress_postcode: form.office_adress_postcode,
        office_VAT_Num: form.office_VAT_Num,
        office_telephone: form.office_telephone,
        office_email: form.office_email,
        office_SoMe_One: form.office_SoMe_One,
        office_SoMe_Two: form.office_SoMe_Two,
        office_SoMe_Three: form.office_SoMe_Three,
        office_SoMe_Four: form.office_SoMe_Four,
        office_SoMe_Five: form.office_SoMe_Five,
        office_SoMe_Six: form.office_SoMe_Six,
        office_language: form.office_language,
        }

        console.log('data =>',data)

        if(form.office_city_location === undefined || form.office_country_location === undefined) return alert(`Please fill in all fields`)
        console.log('form before try', form)
        
        try {
            const response = await axios.post(`${url}/offices/add`, data)
            console.log('response after try', response)

            let tempMsg = response.data.message.slice(0, 6)
            if(response.data.ok === false && tempMsg === 'E11000') {
                
                return alert(`Error! officeID has to be unique, an office with officeID:${form.officeID} already exists in the database.`)
            }
            else if(response.data.ok === true) {
                
                return alert(`office with officeID: ${form.officeID} and name: ${form.office_firstname} has been created in the database.`)
            }
            
            
        }catch(error){
            console.log(error)
            
          
        }
    }
    return (
        <div >
            <form onSubmit={handleSubmit} className="CRUD-form" >
                <h3>Adding a new office</h3>
                <div>* = required fields</div>

                *<p>Office Name:</p> 
                                    <input 
                                    required
                                    name='office_firstname'
                                    onChange={handleFormChange}
                                    value={form.office_firstname}
                                    placeholder=''/>
                <p>Office ID:</p>
                                    <input name='officeID'
                                    onChange={handleFormChange}
                                    value={form.officeID}
                                    placeholder=''/> 
                <p>Office Image:</p>
                                    <input name='office_image'
                                    onChange={handleFormChange}
                                    value={form.office_image}
                                    placeholder=''/>
                <p>Office Logo:</p>
                                    <input name='office_logo'
                                    onChange={handleFormChange}
                                    value={form.office_logo}
                                    placeholder=''/>
                <p>Office Subname:</p>
                                    <input name='office_middlename'
                                    onChange={handleFormChange}
                                    value={form.office_middlename}
                                    placeholder=''/>
                <p>Office Lastname:</p>
                                    <input name='office_lastname'
                                    onChange={handleFormChange}
                                    value={form.office_lastname}
                                    placeholder=''/>
                <p>Office Country Location:</p>
                                    <select required onChange={handleFormChange} name='office_country_location'>
                                        <option value="default">- choose country office city location -</option>
                                        <option value="Denmark">Denmark</option>
                                        <option value="Sweden">Sweden</option>
                                        <option value="Norway">Norway</option>
                                        <option value="Estonia">Estonia</option>
                                    </select>
                <p>Office City Location:</p>
                                    <input 
                                    required
                                    name='office_city_location'
                                    onChange={handleFormChange}
                                    value={form.office_city_location}
                                    placeholder=''/>
                <p>Office Location:</p>
                                    <input name='office_location'
                                    onChange={handleFormChange}
                                    value={form.office_location}
                                    placeholder=''/>
                <p>Office Adress Street:</p>
                                    <input name='office_adress_street'
                                    onChange={handleFormChange}
                                    value={form.office_adress_street}
                                    placeholder=''/>
                <p>Office Adress Number:</p>
                                    <input name='office_adress_number'
                                    onChange={handleFormChange}
                                    value={form.office_adress_number}
                                    placeholder=''/>
                <p>Office Adress Postcode:</p>
                                    <input name='office_adress_postcode'
                                    onChange={handleFormChange}
                                    value={form.office_adress_postcode}
                                    placeholder=''/>
                <p>Office VAT number:</p>
                                    <input name='office_VAT_Num'
                                    onChange={handleFormChange}
                                    value={form.office_VAT_Num}
                                    placeholder=''/>
                <p>Office Telephone:</p>
                                    <input name='office_telephone'
                                    onChange={handleFormChange}
                                    value={form.office_telephone}
                                    placeholder=''/>
                <p>Office Email:</p>
                                    <input name='office_email'
                                    onChange={handleFormChange}
                                    value={form.office_email}
                                    placeholder=''/>
                <p>Office SoMe One:</p>
                                    <input name='office_SoMe_One'
                                    onChange={handleFormChange}
                                    value={form.office_SoMe_One}
                                    placeholder=''/>
                <p>Office SoMe Two:</p>
                                    <input name='office_SoMe_Two'
                                    onChange={handleFormChange}
                                    value={form.office_SoMe_Two}
                                    placeholder=''/>
                <p>Office SoMe Three:</p>
                                    <input name='office_SoMe_Three'
                                    onChange={handleFormChange}
                                    value={form.office_SoMe_Three}
                                    placeholder=''/>
                <p>Office SoMe Four:</p>
                                    <input name='office_SoMe_Four'
                                    onChange={handleFormChange}
                                    value={form.office_SoMe_Four}
                                    placeholder=''/>
                <p>Office SoMe Five:</p>
                                    <input name='office_SoMe_Five'
                                    onChange={handleFormChange}
                                    value={form.office_SoMe_Five}
                                    placeholder=''/>
                <p>Office SoMe Six:</p>
                                    <input name='office_SoMe_Six'
                                    onChange={handleFormChange}
                                    value={form.office_SoMe_Six}
                                    placeholder=''/>
                <p>Office language:</p>
                                    <input name='office_language'
                                    onChange={handleFormChange}
                                    value={form.office_language}
                                    placeholder=''/>

                <button>Add new office</button>
            </form>
        





        </div>
    )
}

export default OfficeAdd