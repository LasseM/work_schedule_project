import React, { useState } from 'react'
import { url } from '../../../config'
import axios from 'axios'



const OfficeFind = () => {


 const [form, setForm] = useState('')
  //========Find office
  const [findOffice, setFindOffice] = useState([])


 
  const handleFormChange = (e) => {
      // console.log(e.target.name, e.target.value)
      setForm({...form,[e.target.name]:e.target.value})
  }


//====== HandleSubmit ====== 
const handleSubmit = async (e) => {
  e.preventDefault()

try {
    const response = await axios.get(`${url}/offices/find_offices`)
      const temp = []
      
      console.log("response:", response)

      response.data.response.forEach((ele) => {

      temp.push(ele)
      
      });
      setFindOffice([...temp])
      console.log('temp:', temp)
}catch(error){
    console.log(error)
}
}
    return (
        <div className="CRUD-form">
          <h3>Here you can search for all offices</h3>
          <form onSubmit={handleSubmit}>

      
        <div>
          <p>*Choose office:</p>                        
          <select onChange={handleFormChange} name='office_type'>
                  <option value="nothing"> - choose office - </option>
                  <option value="All offices"> - All offices - </option>
                  <option value='Copenhagen'>Copenhagen</option>
                  <option value='Oslo'>Oslo</option>
                  <option value='Stockholm'>Stockholm</option>
                  <option value='Tallinn'>Tallinn</option>

          </select>
        </div>
          <button>Search</button>
          </form>

{/* ======= Return of the search ====== */}

{
            findOffice.map( (ele, idx) =>{
           return  <div key={idx} >
    <h2>Search results for {form.office_type}:</h2>
    <div className="returnedOffices">
      <p className="list">Number of search results:</p> <p className="resultInLists">{idx + 1} of {findOffice.length}</p>
      <p className="list">office_firstname:</p> 
      <input 
      className="resultInLists"
      value={ele.office_firstname}
      readOnly="true"
      />
      <p className="list">systemID:</p> 
      <input 
      className="resultInLists"
      value={ele._id}
      readOnly="true"
      />
      <p className="list">officeID:</p> 
      <input 
      className="resultInLists"
      value={ele.officeID}
      readOnly="true"
      /> 
      <p className="list">office_image:</p> 
      <input 
      className="resultInLists"
      value={ele.office_image}
      readOnly="true"
      /> 
      <p className="list">office_logo:</p> 
      <input 
      className="resultInLists"
      value={ele.office_logo}
      readOnly="true"
      /> 
      <p className="list">office_middlename:</p> 
      <input 
      className="resultInLists"
      value={ele.office_middlename}
      readOnly="true"
      /> 
      <p className="list">office_lastname:</p> 
      <input 
      className="resultInLists"
      value={ele.office_lastname}
      readOnly="true"
      /> 
      <p className="list">office_country_location:</p> 
      <input 
      className="resultInLists"
      value={ele.office_country_location}
      readOnly="true"
      /> 
      <p className="list">office_city_location:</p> 
      <input 
      className="resultInLists"
      value={ele.office_city_location}
      readOnly="true"
      /> 
      <p className="list">office_location:</p> 
      <input 
      className="resultInLists"
      value={ele.office_location}
      readOnly="true"
      /> 
      <p className="list">office_adress_street:</p> 
      <input 
      className="resultInLists"
      value={ele.office_adress_street}
      readOnly="true"
      /> 
      <p className="list">office_adress_number:</p> 
      <input 
      className="resultInLists"
      value={ele.office_adress_number}
      readOnly="true"
      /> 
      <p className="list">office_adress_postcode:</p> 
      <input 
      className="resultInLists"
      value={ele.office_adress_postcode}
      readOnly="true"
      /> 
      <p className="list">office_VAT_Num:</p> 
      <input 
      className="resultInLists"
      value={ele.office_VAT_Num}
      readOnly="true"
      /> 
      <p className="list">office_telephone:</p> 
      <input 
      className="resultInLists"
      value={ele.office_telephone}
      readOnly="true"
      /> 
      <p className="list">office_email:</p> 
      <input 
      className="resultInLists"
      value={ele.office_email}
      readOnly="true"
      /> 
      <p className="list">office_SoMe_One:</p> 
      <input 
      className="resultInLists"
      value={ele.office_SoMe_One}
      readOnly="true"
      /> 
      <p className="list">office_SoMe_Two:</p> 
      <input 
      className="resultInLists"
      value={ele.office_SoMe_Two}
      readOnly="true"
      /> 
      <p className="list">office_SoMe_Three:</p> 
      <input 
      className="resultInLists"
      value={ele.office_SoMe_Three}
      readOnly="true"
      /> 
      <p className="list">office_SoMe_Four:</p> 
      <input 
      className="resultInLists"
      value={ele.office_SoMe_Four}
      readOnly="true"
      /> 
      <p className="list">office_SoMe_Five:</p> 
      <input 
      className="resultInLists"
      value={ele.office_SoMe_Five}
      readOnly="true"
      /> 
      <p className="list">office_SoMe_Six:</p> 
      <input 
      className="resultInLists"
      value={ele.office_SoMe_Six}
      readOnly="true"
      /> 
      <p className="list">office_language:</p> 
      <input 
      className="resultInLists"
      value={ele.office_language}
      readOnly="true"
      /> 
    </div>
           </div>
            })
          }

{/* ================== */}
        </div>
    )
}

export default OfficeFind