import React, { useState } from 'react'
import { url } from '../../../config'
import axios from 'axios'
import OfficeFindOne from './find_one_office'
import EmptyFormOfficeContent from './emptyFormOfficeContent'

const OfficeRemove = () => {
    const [inputsFromSearch, setInputsFromSearch] = useState({})
    const [form, setForm] = useState(EmptyFormOfficeContent)

    // ====== Removed office 
    // const [removedoffice, setRemovedoffice] = useState('')

    const input = (res) => {
        // console.log("res from input =>", res)
        setInputsFromSearch({inputsFromSearch, ...res})

    }


    const handleFormChange = (e) => {
        // console.log(e.target.name, e.target.value)
        setForm({...form,[e.target.name]:e.target.value})
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
    
    // setRemovedoffice([...removedoffice, data])
    // console.log('removedoffice =>', removedoffice)
        console.log('inputsFromSearch._id after submit', inputsFromSearch._id)
    try {
        
        const response = await axios.delete(`${url}/offices/remove/${inputsFromSearch._id}`)
        console.log('response after await inside Remove', response)
        alert(`office with system id: >> ${inputsFromSearch._id} << and name: ${inputsFromSearch.office_firstname} has been deleted from database`)
    }catch(error){
        console.log('error in catch in remove in client')
        alert('Something went wrong')
    }
}

    return (
        <div>

        <OfficeFindOne input={input} />

         <form 
         onSubmit={handleSubmit} 
         className="CRUD-form" 
         >
            <h3>Delete an office</h3>

            Office _id:<input name='_id'
                                onChange={handleFormChange}
                                value={inputsFromSearch._id}
                                placeholder={inputsFromSearch._id}/> 
            <button>Remove office</button>
        </form>



        </div>
    )
}

export default OfficeRemove