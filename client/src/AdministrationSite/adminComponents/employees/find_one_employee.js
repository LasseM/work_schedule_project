import React, { useState, useEffect } from 'react'
import { url } from '../../../config'
import axios from 'axios'


const EmployeeFindOne = (props) => {


  const [form, setForm] = useState('')
  const [findOneEmployee, setFindOneEmployee] = useState([])
  const [notThere, setNotThere] = useState(false)
  
  // ====== What to render ======
  const [renderAll, setRenderAll] = useState(true)
  useEffect( () => {
    props.hideRender
    ? setRenderAll(false)
    : setRenderAll(true)
  }, [])

  const handleFormChange = (e) => {
    console.log(e.target.name, e.target.value)
    setForm({...form,[e.target.name]:e.target.value})
  }

  const handleSubmit = async (e) => {
    e.preventDefault()

    console.log("form.employee_firstname =>", form.employee_firstname)
    if(form.employee_firstname === undefined && form._id === undefined) return alert(`Please fill in either Employee Name or Employee System ID before search`);

    
    const data = {
      employee_firstname: form.employee_firstname,
      _id: form._id,
      
    }
    
    try {
      const response = await axios.post(`${url}/employees/find_one_employee`, data)
      console.log("response", response)
      
      

    if(response.data.ok) {
      let current = findOneEmployee
      current =[response.data.response]
      setFindOneEmployee([...current])
      props.input(response.data.response, "findondesearch")
      setNotThere(false) 
    } else {
      setNotThere(true)
      setFindOneEmployee([])
     }
    

  }catch(error){
      console.log(error)
  }
  }
      return (
          <div className="CRUD-form">
            <p>Here you can search for one specific employees</p>
            <form onSubmit={handleSubmit} className="onSubmitForm">
            

            <select>
              <option>- Employee Type -</option>
              <option>MICE</option>
              <option>Cruise</option>
            </select>
            
            
            {/* {
              
              "/employee_site/employees/employee_remove" ?   
              <div>
                <p>Employee Object_id:</p>
                <input name='_id'
                onChange={handleFormChange}
                value={form._id}
                placeholder=''/> 
              </div>
              : */}
              <div>
                <p className="list">Employee Firstname:</p>
                <input name='employee_firstname'
                            onChange={handleFormChange}
                            value={form.employee_firstname}
                            placeholder=''/> 
          
                <p className="list">Employee Object_id:</p>
                <input name='_id'
                            onChange={handleFormChange}
                            value={form._id}
                            placeholder=''/> 
              </div>
            {/* }  */}
                

            <button>Search</button>
            </form>

      {
        renderAll
        ? findOneEmployee.length !== 0
          ? findOneEmployee.map( (ele, idx) =>{
                    
                    
                    console.log('ele from UpdateOneEmployee:', ele)
                return <div key={idx} className="returnedEmployees">
      <p className="list">Employee system ID:</p>
      <input
      name="ele._id"
      value={ele._id }
      readOnly='true'
      />
      <p className="list">employeeID:</p>
      <input
      name="ele.employeeID"
      value={ele.employeeID} 
      readOnly='true'
      />
      <p className="list">employee_password:</p>
      <input
      type="password"
      name="ele.employee_password"
      value={ele.employee_password} 
      readOnly='true'
      />
      <p className="list">employee_type:</p>
      <input
      name="ele.employee_type"
      value={ele.employee_type} 
      readOnly='true'
      />
      <p className="list">employee_image:</p>
      {/* <input
      name="ele.employee_image"
      value={ele.employee_image} 
      readOnly='true'
      /> */}
       <img 
          className="profileImg"
          alt='employee_image'
          // onChange={handleFormChange}
          src={ele.employee_image.photo_url}
          //placeholder=''
          />
      <p className="list">employee_firstname:</p>
      <input
      name="ele.employee_firstname"
      value={ele.employee_firstname} 
      readOnly='true'
      />
      <p className="list">employee_middlename:</p>
      <input
      name="ele.employee_middlename"
      value={ele.employee_middlename} 
      readOnly='true'
      />
      <p className="list">employee_lastname:</p>
      <input
      name="ele.employee_lastname"
      value={ele.employee_lastname} 
      readOnly='true'
      />
      <p className="list">employee_email:</p>
      <input
      name="ele.employee_email"
      value={ele.employee_email} 
      readOnly='true'
      />
      <p className="list">employee_addressStreet:</p>
      <input
      name="ele.employee_addressStreet"
      value={ele.employee_addressStreet} 
      readOnly='true'
      />
      <p className="list">employee_addressNumber:</p>
      <input
      name="ele.employee_addressNumber"
      value={ele.employee_addressNumber} 
      readOnly='true'
      />
      <p className="list">employee_postalCode:</p>
      <input
      name="ele.employee_postalCode"
      value={ele.employee_postalCode} 
      readOnly='true'
      />
      <p className="list">employee_city:</p>
      <input
      name="ele.employee_city"
      value={ele.employee_city} 
      readOnly='true'
      />
      <p className="list">employee_country:</p>
      <input
      name="ele.employee_country"
      value={ele.employee_country} 
      readOnly='true'
      />
      <p className="list">employee_phone:</p>
      <input
      name="ele.employee_phone"
      value={ele.employee_phone} 
      readOnly='true'
      />
      <p className="list">employee_mobile:</p>
      <input
      name="ele.employee_mobile"
      value={ele.employee_mobile} 
      readOnly='true'
      />
      <p className="list">employee_office:</p>
      <input
      name="ele.employee_office"
      value={ele.employee_office} 
      readOnly='true'
      />
      <p className="list">employee_position:</p>
      <input
      name="ele.employee_position"
      value={ele.employee_position} 
      readOnly='true'
      />
      <p className="list">employee_focusarea:</p>
      <input
      name="ele.employee_focusarea"
      value={ele.employee_focusarea} 
      readOnly='true'
      />
      <p className="list">employee_nationality:</p>
      <input
      name="ele.employee_nationality"
      value={ele.employee_nationality} 
      readOnly='true'
      />
      <p className="list">employee_language:</p>
      <input
      name="ele.employee_language"
      value={ele.employee_language} 
      readOnly='true'
      />
      <p className="list">employee_bank_reg:</p>
      <input
      name="ele.employee_bank_reg"
      value={ele.employee_bank_reg} 
      readOnly='true'
      />
      <p className="list">employee_bank_account_number:</p>
      <input
      name="ele.employee_bank_account_number"
      value={ele.employee_bank_account_number} 
      readOnly='true'
      />
      <p className="list">employee_payroll_num:</p>
      <input
      name="ele.employee_payroll_num"
      value={ele.employee_payroll_num} 
      readOnly='true'
      />
      <p className="list">employee_tax_card:</p>
      <input
      name="ele.employee_tax_card"
      value={ele.employee_tax_card} 
      readOnly='true'
      />


                </div>
                  }):<div>Input not recognized</div>
      : null
      }
      

          </div>
      )
}


export default EmployeeFindOne