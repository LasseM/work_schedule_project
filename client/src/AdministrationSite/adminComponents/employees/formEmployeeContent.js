const formEmployeeContent = {

                employeeID: '12',
                employee_password: '1234',
                employeeType:  '',
                employee_image: {
                    photo_url: "https://res.cloudinary.com/dkbo4lvxt/image/upload/v1575997495/lwfxabdouj6qmz2tembz.jpg",
                    public_id: "lwfxabdouj6qmz2tembz",
                },
                employee_firstname: 'Peter',
                employee_middlename: '',
                employee_lastname: 'Hansen',
                employee_email: 'ph@mail.com',
                employee_addressStreet: 'Gade',
                employee_addressNumber: '23',
                employee_postalCode: '2300',
                employee_city: 'København',
                employee_country: '',
                employee_phone: '+45 9990 9999',
                employee_mobile: '',
                employee_office: '',
                employee_position: '',
                employee_focusarea: '',
                employee_nationality: 'Danish',
                employee_language: '',
                employee_bank_reg: '4545',
                employee_bank_account_number: '36478593',
                employee_payroll_num: '',
                employee_tax_card: '',

}
export default formEmployeeContent