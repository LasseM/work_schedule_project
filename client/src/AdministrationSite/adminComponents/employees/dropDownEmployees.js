import React, { useState, useEffect } from 'react'
import { url } from '../../../config'
import axios from 'axios'




const DropDownEmployees = (props) => {

    
    console.log('props ==>', props)

    //========Find employee
    const [findEmployee, setFindEmployee] = useState([])

    const [inputsFromSearch, setInputsFromSearch] = useState([])

    const [chosenEmployeesClicked, setChosenEmployeesClicked] = useState(false)

    useEffect(() => {
        GetEmployees()
    }, [])

    //====== HandleSubmit ====== 
    const GetEmployees = async () => {

        try {
            const response = await axios.get(`${url}/employees/find_employees`)
            const temp = []
            var BasicInfo = {}
            console.log("response ===>", response)

            response.data.response.forEach((ele) => {

                if (ele.employee_type === 'Freelancer') {

                    BasicInfo = {
                        employee_firstname: ele.employee_firstname,
                        employee_lastname: ele.employee_lastname,
                        _id: ele._id,
                    }
                    temp.push(BasicInfo)
                    console.log("temp:", temp)
                }
            });
            setFindEmployee([...temp])
            console.log('temp:', temp)

        } catch (error) {
            console.log(error)
        }

    }






    console.log('findEmployee after try =>', findEmployee)

    return (
        <div className="CRUD-form">

            {/* <h2>Search results for employees:</h2> */}

            <select 
            class="select" 
            multiple="multiple"
            >
                {

                    findEmployee.map((item, idx) => {
                        // debugger
                        return <option className="chosenEmployees" onClick={() => props.chosenEmployees(item, idx)} value={idx}>{item.employee_firstname} {item.employee_lastname}</option>
                    })
                }
            </select>

        </div>
    )
}

export default DropDownEmployees

//JSON.stringify(item[1])


{/* return <select key={i}> */ }
{/* <option>Hello</option> */ }
{/* </select> */ }















