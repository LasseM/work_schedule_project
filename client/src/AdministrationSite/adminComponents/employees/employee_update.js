import React, { useState, useEffect } from 'react'
import EmptyFormEmployeeContent from './emptyFormEmployeeContent'
import { url } from '../../../config'
import axios from 'axios'
import EmployeeFindOne from './find_one_employee'

const EmployeeUpdate = (props) => {
    const [initialInput, setInitialInput] = useState({})
    const [inputsFromSearch, setInputsFromSearch] = useState({})
    const [form, setForm] = useState(EmptyFormEmployeeContent)
    
console.log('props UU=>', props)

// ====== Render Headings ======
const [renderHeadings, setRenderHeadings] = useState(true)
useEffect( () => {
   
return props.hideheadingsMyProfile
? (setRenderHeadings(false), setInputsFromSearch({...props.MyProfileForm}))
: setRenderHeadings(true)
}, [props.MyProfileForm])



    const input = (res) => {
        setInputsFromSearch({inputsFromSearch, ...res})
        setInitialInput({...inputsFromSearch, ...res})
    }

    
    const handleFormChange = (e) => {
        setInputsFromSearch({...inputsFromSearch,[e.target.name]:e.target.value})
    }
    
    const CancelChange = (res) => {
        setInputsFromSearch({...inputsFromSearch, ...initialInput})
      }
console.log('inputsFromSearch before submit', inputsFromSearch)

    const handleSubmit = async (e) => {
        e.preventDefault()

        const data = {
            _id: inputsFromSearch._id,
            employeeID: inputsFromSearch.employeeID,
            // employee_password: inputsFromSearch.employee_password,
            // employee_password2: inputsFromSearch.employee_password2,
            employee_type: inputsFromSearch.employee_type,
            employee_image: inputsFromSearch.employee_image,
            employee_firstname: inputsFromSearch.employee_firstname,
            employee_middlename: inputsFromSearch.employee_middlename,
            employee_lastname: inputsFromSearch.employee_lastname,
            employee_email: inputsFromSearch.employee_email,
            employee_addressStreet: inputsFromSearch.employee_addressStreet,
            employee_addressNumber: inputsFromSearch.employee_addressNumber,
            employee_postalCode: inputsFromSearch.employee_postalCode,
            employee_city: inputsFromSearch.employee_city,
            employee_country: inputsFromSearch.employee_country,
            employee_phone: inputsFromSearch.employee_phone,
            employee_mobile: inputsFromSearch.employee_mobile,
            employee_office: inputsFromSearch.employee_office,
            employee_position: inputsFromSearch.employee_position,
            employee_focusarea: inputsFromSearch.employee_focusarea,
            employee_nationality: inputsFromSearch.employee_nationality,
            employee_language: inputsFromSearch.employee_language,
            employee_bank_reg: inputsFromSearch.employee_bank_reg,
            employee_bank_account_number: inputsFromSearch.employee_bank_account_number,
            employee_payroll_num: inputsFromSearch.employee_payroll_num,
            employee_tax_card: inputsFromSearch.employee_tax_card,
        }
        
        console.log('data =>', data)

        if(inputsFromSearch.employee_type === '' || inputsFromSearch.employee_country_location === '') return alert(`Please fill in all fields`)
        console.log('form before try', form)
        
        try {
            const response = await axios.put(`${url}/employees/update_employee`, data)
            console.log('response =>', response)
            alert(`Employee with id: >> ${inputsFromSearch._id} << and name: ${inputsFromSearch.employee_firstname} ${inputsFromSearch.employee_lastname} has been updated`)
        }catch(error){
            console.log(error)
            alert(error.data.message)
        }
    }
    return (
        <div >
             { 
                renderHeadings
                ?
                    <EmployeeFindOne input={input} hideRender={true}/>
                : null
             }
            <form onSubmit={handleSubmit} className="CRUD-form">

                { 
                renderHeadings
                ?
                <div>
                <h3>Update the employee below:</h3>
                <div>* = required fields</div>
                </div>
                :null
                }   
                <p>Employee firstname:</p> 
                                    <input 
                                    required
                                    name='employee_firstname'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_firstname}
                                    placeholder=''/>
                <p>Employee D:</p> 
                                    <input
                                    name='employeeID'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employeeID}
                                    placeholder=''
                                    />
                 { 
                renderHeadings
                ?
                <span>
                *<p>Employee password:</p> 
                                    <input
                                    required
                                    name='employee_password'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_password}
                                    placeholder=''/>
                </span>
                : null
                }
                 { 
                renderHeadings
                ?
                <span>
                <p>*Employee type:</p> 
                                    <select required onChange={handleFormChange} name='employee_type'>
                                        <option value="default">- choose employee type -</option>
                                        <option value="Freelancer">Freelancer</option>
                                        <option value="Full-Time">Full-Time Employee</option>
                                        <option value="Admin">Admin</option>
                                    </select>
                </span>
                : null
                }
                <p>Employee image:</p> {
                                        inputsFromSearch.employee_image === undefined
                                        ?
                                        <img src={'https://res.cloudinary.com/dkbo4lvxt/image/upload/v1575989812/kkf1hi5dsyvjdvzex1sw.jpg'}
                                        className="Default profileImg"
                                        alt="Default profile img"
                                        />
                                        : <img src={inputsFromSearch.employee_image.photo_url}
                                        className="profileImg"
                                        alt="profile img"
                                        />
                                    }   
                                    {/* <img 
                                    className="profileImg"
                                    alt='employee_image'
                                    src={'https://res.cloudinary.com/dkbo4lvxt/image/upload/v1575997495/lwfxabdouj6qmz2tembz.jpg'}
                                    /> */}
                <p>Employee middlename:</p> 
                                    <input name='employee_middlename'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_middlename}
                                    placeholder=''/>
                <p>*Employee lastname:</p> 
                                    <input 
                                    required
                                    name='employee_lastname'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_lastname}
                                    placeholder=''/>
                <p>Employee email:</p> 
                                    <input name='employee_email'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_email}
                                    placeholder=''/>
                <p>Employee addressStreet:</p> 
                                    <input name='employee_addressStreet'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_addressStreet}
                                    placeholder=''/>
                <p>Employee addressNumber:</p> 
                                    <input name='employee_addressNumber'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_addressNumber}
                                    placeholder=''/>
                <p>Employee postalCode:</p> 
                                    <input name='employee_postalCode'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_postalCode}
                                    placeholder=''/>
                <p>Employee city:</p> 
                                    <input name='employee_city'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_city}
                                    placeholder=''/>
                <p>Employee country:</p> 
                                    <select required onChange={handleFormChange} name='employee_country'>
                                        <option value="default">- choose country event_city_location -</option>
                                        <option value="Denmark">Denmark</option>
                                        <option value="Sweden">Sweden</option>
                                        <option value="Norway">Norway</option>
                                        <option value="Estonia">Estonia</option>
                                    </select>
                <p>Employee phone:</p> 
                                    <input name='employee_phone'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_phone}
                                    placeholder=''/>
                <p>Employee mobile:</p> 
                                    <input name='employee_mobile'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_mobile}
                                    placeholder=''/>
                <p>Employee office:</p> 
                                    <select required onChange={handleFormChange} name='event_country_location'>
                                        <option value="default">- choose office -</option>
                                        <option value="Copenhagen">Copenhagen</option>
                                        <option value="Stockholm">Stockholm</option>
                                        <option value="Oslo">Bergen</option>
                                        <option value="Tallinn">Tallinn</option>
                                    </select>
                <p>Employee position:</p> 
                                    <input name='employee_position'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_position}
                                    placeholder=''/>
                <p>Employee focusarea:</p> 
                                    <input name='employee_focusarea'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_focusarea}
                                    placeholder=''/>
                <p>Employee nationality:</p> 
                                    <input name='employee_nationality'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_nationality}
                                    placeholder=''/>
                <p>Employee language:</p> 
                                    <input name='employee_language'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_language}
                                    placeholder=''/>
                <p>Employee bank_reg:</p> 
                                    <input name='employee_bank_reg'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_bank_reg}
                                    placeholder=''/>
                <p>Employee bank_account_number:</p> 
                                    <input name='employee_bank_account_number'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_bank_account_number}
                                    placeholder=''/>
                <p>Employee payroll_num:</p> 
                                    <input name='employee_payroll_num'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_payroll_num}
                                    placeholder=''/>
                <p>Employee tax_card:</p> 
                                    <input name='employee_tax_card'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.employee_tax_card}
                                    placeholder=''/>
            <button>Update the Employee</button>
            </form>

            <button className="cancelChange" onClick={CancelChange}>Cancel Changes</button>
        </div>

    )
}


export default EmployeeUpdate