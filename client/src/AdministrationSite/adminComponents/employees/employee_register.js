import React, { useState} from 'react'
import formEmployeeContent from './formEmployeeContent'
import { url } from '../../../config'
import axios from 'axios'
import UploadImages from '../../../components/UploadImages'

const EmployeeRegister = () => {



    const [form, setForm] = useState(formEmployeeContent)

    const handleFormChange = (e) => {
        console.log("formchange", e.target.name, e.target.value)
        setForm({...form,[e.target.name]:e.target.value})
    }


    const getImages =(img) => {
        setForm({...form, employee_image:img})
    }
    const handleSubmit = async (e) => {
        e.preventDefault()
        
        const data = {
 
            employeeID: form.employeeID,
            employee_password: form.employee_password,
            employee_password2: form.employee_password2,
            employee_type: form.employee_type,
            employee_image: form.employee_image,
            employee_firstname: form.employee_firstname,
            employee_middlename: form.employee_middlename,
            employee_lastname: form.employee_lastname,
            employee_email: form.employee_email.toLowerCase(),
            employee_addressStreet: form.employee_addressStreet,
            employee_addressNumber: form.employee_addressNumber,
            employee_postalCode: form.employee_postalCode,
            employee_city: form.employee_city,
            employee_country: form.employee_country,
            employee_phone: form.employee_phone,
            employee_mobile: form.employee_mobile,
            employee_office: form.employee_office,
            employee_position: form.employee_position,
            employee_focusarea: form.employee_focusarea,
            employee_nationality: form.employee_nationality,
            employee_language: form.employee_language,
            employee_bank_reg: form.employee_bank_reg,
            employee_bank_account_number: form.employee_bank_account_number,
            employee_payroll_num: form.employee_payroll_num,
            employee_tax_card: form.employee_tax_card,


        }

        // console.log('data =>',data)

        if(form.employee_firstname === undefined || form.employee_lastname === undefined || form.employee_type === undefined || form.employee_country === undefined) return alert(`Please fill in all fields`)
        // console.log('form before try', form)
        
        try {
            const response = await axios.post(`${url}/employees/register`, data)
            console.log('response after try', response)

            // let tempMsg = response.data.message.slice(0, 6)
            // console.log('tempMsg =>', tempMsg)
            // if(response.data.ok === false && tempMsg === 'E11000') {
                
            //     return alert(`Error! EmployeeID has to be unique, an employee with EmployeeID:${form.employeeID} already exists in the database.`)
            // }
            // else 
            
            if(response.data.ok === true) {
                
                return alert(`Employee with EmployeeID: ${form.employeeID} and name: ${form.employee_firstname} ${form.employee_lastname} has been created in the database.`)
            }
            
            
        }catch(error){
            console.log(error)

          
        }
    }
    return (
        <div>
            <form onSubmit={handleSubmit} className="CRUD-form">
                <h3>Adding a new employee</h3>
                <div>* = required fields</div>

                employee_firstname: <input 
                                    required
                                    name='employee_firstname'
                                    onChange={handleFormChange}
                                    value={form.employee_firstname}
                                    placeholder=''/>
                employeeID: <input
                                    name='employeeID'
                                    onChange={handleFormChange}
                                    value={form.employeeID}
                                    placeholder=''
                                    />
                *employee_password: <input
                                    required
                                    name='employee_password'
                                    onChange={handleFormChange}
                                    value={form.employee_password}
                                    placeholder=''/>
                *employee_password2: <input
                                    required
                                    name='employee_password2'
                                    onChange={handleFormChange}
                                    value={form.employee_password2}
                                    placeholder=''/>
                *employee_type: <select required onChange={handleFormChange} name='employee_type'>
                        <option value="default">- choose employee type -</option>
                        <option value="Freelancer">Freelancer</option>
                        <option value="Full-Time">Full-Time Employee</option>
                        <option value="Admin">Admin</option>
                        </select>
                employee_image: <img 
                                    className="profileImg"
                                    alt='employee_image'
                                    // onChange={handleFormChange}
                                    src={form.employee_image.photo_url}
                                    //placeholder=''
                                    />
                                    
                employee_middlename: <input name='employee_middlename'
                                    onChange={handleFormChange}
                                    value={form.employee_middlename}
                                    placeholder=''/>
                *employee_lastname: <input 
                                    required
                                    name='employee_lastname'
                                    onChange={handleFormChange}
                                    value={form.employee_lastname}
                                    placeholder=''/>
                employee_email: <input name='employee_email'
                                    onChange={handleFormChange}
                                    value={form.employee_email}
                                    placeholder=''/>
                employee_addressStreet: <input name='employee_addressStreet'
                                    onChange={handleFormChange}
                                    value={form.employee_addressStreet}
                                    placeholder=''/>
                employee_addressNumber: <input name='employee_addressNumber'
                                    onChange={handleFormChange}
                                    value={form.employee_addressNumber}
                                    placeholder=''/>
                employee_postalCode: <input name='employee_postalCode'
                                    onChange={handleFormChange}
                                    value={form.employee_postalCode}
                                    placeholder=''/>
                employee_city: <input name='employee_city'
                                    onChange={handleFormChange}
                                    value={form.employee_city}
                                    placeholder=''/>
                employee_country: <select required onChange={handleFormChange} name='employee_country'>
                        <option value="default">- choose country event_city_location -</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Norway">Norway</option>
                        <option value="Estonia">Estonia</option>
                        </select>
                employee_phone: <input name='employee_phone'
                                    onChange={handleFormChange}
                                    value={form.employee_phone}
                                    placeholder=''/>
                employee_mobile: <input name='employee_mobile'
                                    onChange={handleFormChange}
                                    value={form.employee_mobile}
                                    placeholder=''/>
                employee_office: <select required onChange={handleFormChange} name='event_country_location'>
                        <option value="default">- choose office -</option>
                        <option value="Copenhagen">Copenhagen</option>
                        <option value="Stockholm">Stockholm</option>
                        <option value="Oslo">Bergen</option>
                        <option value="Tallinn">Tallinn</option>
                        </select>
                employee_position: <input name='employee_position'
                                    onChange={handleFormChange}
                                    value={form.employee_position}
                                    placeholder=''/>
                employee_focusarea: <input name='employee_focusarea'
                                    onChange={handleFormChange}
                                    value={form.employee_focusarea}
                                    placeholder=''/>
                employee_nationality: <input name='employee_nationality'
                                    onChange={handleFormChange}
                                    value={form.employee_nationality}
                                    placeholder=''/>
                employee_language: <input name='employee_language'
                                    onChange={handleFormChange}
                                    value={form.employee_language}
                                    placeholder=''/>
                employee_bank_reg: <input name='employee_bank_reg'
                                    onChange={handleFormChange}
                                    value={form.employee_bank_reg}
                                    placeholder=''/>
                employee_bank_account_number: <input name='employee_bank_account_number'
                                    onChange={handleFormChange}
                                    value={form.employee_bank_account_number}
                                    placeholder=''/>
                employee_payroll_num: <input name='employee_payroll_num'
                                    onChange={handleFormChange}
                                    value={form.employee_payroll_num}
                                    placeholder=''/>
                employee_tax_card: <input name='employee_tax_card'
                                    onChange={handleFormChange}
                                    value={form.employee_tax_card}
                                    placeholder=''/>
            <button>Add new Employee</button>
            </form>
            <UploadImages getImages={getImages}/>
        </div>
    )
}

export default EmployeeRegister