import React, { useState, useEffect } from 'react'
import { url } from '../../../config'
import axios from 'axios'


const EmployeeFind = () => {


  const [form, setForm] = useState('')
  //========Find employee
  const [findEmployee, setFindEmployee] = useState([])


  const handleFormChange = (e) => {
    // console.log(e.target.name, e.target.value)
    setForm({ ...form, [e.target.name]: e.target.value })
  }



  // ====== Toggle Handle ====== //


  const handleToggle = (idx) => {

    const toggleVar = [...findEmployee]

    toggleVar[idx].toggle = !toggleVar[idx].toggle

    setFindEmployee([...toggleVar])

  }

  //====== HandleSubmit ====== 
  const handleSubmit = async (e) => {
    e.preventDefault()

    try {
      const response = await axios.get(`${url}/employees/find_employees`)
      const temp = []

      console.log("response:", response)

      response.data.response.forEach((ele) => {
        ele.toggle = false
        if (ele.employee_type === form.employee_type) {
          temp.push(ele)
          console.log("temp:", temp)
        } else if (form.employee_type === 'All Employee Types') {
          temp.push(ele)
          console.log("temp:", temp)
        }
      });
      setFindEmployee([...temp])
      console.log('temp:', temp)
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <div className="CRUD-form">
      <h3>Here you can search for all employees</h3>
      <form onSubmit={handleSubmit}>

        <div>
          <p>*Choose Employee Type:</p>
          <select onChange={handleFormChange} name='employee_type'>
            <option value="nothing"> - choose employee type - </option>
            <option value="All Employee Types"> - All Employee Types - </option>
            <option value='Freelancer'>Freelancer</option>
            <option value='Full-Time'>Full-Time</option>
            <option value='Admin'>Admin</option>
          </select>
        </div>
        <button>Search</button>
      </form>

      {/* ======= Return of the search ====== */}
      <h2>Search results for {form.employee_type}:</h2>

      {
        findEmployee.map((ele, idx) => {
          return <div key={idx} >
            {
              !ele.toggle
                ?
                <div className="returnedEmployees">
                  <p className="list">Number of search results:</p>
                  <p className="resultInLists">{idx + 1} of {findEmployee.length}</p>


                  <p className="list">employee_firstname:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_firstname}
                    readOnly="true"
                  />
                  <button
                    value={idx}
                    onClick={() => handleToggle(idx)}>See all details</button>
                </div>
                :
                <div className="returnedEmployees">
                  <p className="list">Number of search results:</p>
                  <p className="resultInLists">{idx + 1} of {findEmployee.length}</p>
                  <p className="list">employee_firstname:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_firstname}
                    readOnly="true"
                  />
                  <p className="list">employee_email:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_email}
                    readOnly="true"
                  />
                  <p className="list">employee_password:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_password}
                    readOnly="true"
                  />
                  <p className="list">systemID:</p>
                  <input
                    className="resultInLists"
                    value={ele._id}
                    readOnly="true"
                  />
                  <p className="list">employeeID:</p>
                  <input
                    className="resultInLists"
                    value={ele.employeeID}
                    readOnly="true"
                  />
                  <p className="list">employee_type:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_type}
                    readOnly="true"
                  />
                  <p className="list">employee_image:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_image}
                    readOnly="true"
                  />
                  <p className="list">employee_logo:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_logo}
                    readOnly="true"
                  />
                  <p className="list">employee_middlename:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_middlename}
                    readOnly="true"
                  />
                  <p className="list">employee_lastname:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_lastname}
                    readOnly="true"
                  />
                  <p className="list">employee_date:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_date}
                    readOnly="true"
                  />
                  <p className="list">employee_startdate:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_startdate}
                    readOnly="true"
                  />
                  <p className="list">employee_starttime:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_starttime}
                    readOnly="true"
                  />
                  <p className="list">employee_enddate:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_enddate}
                    readOnly="true"
                  />
                  <p className="list">employee_endtime:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_endtime}
                    readOnly="true"
                  />
                  <p className="list">employee_country_location:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_country_location}
                    readOnly="true"
                  />
                  <p className="list">employee_city_location:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_city_location}
                    readOnly="true"
                  />
                  <p className="list">employee_location:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_location}
                    readOnly="true"
                  />
                  <p className="list">employee_adress_street:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_adress_street}
                    readOnly="true"
                  />
                  <p className="list">employee_adress_number:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_adress_number}
                    readOnly="true"
                  />
                  <p className="list">employee_adress_postcode:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_adress_postcode}
                    readOnly="true"
                  />
                  <p className="list">employee_adress_country:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_adress_country}
                    readOnly="true"
                  />
                  <p className="list">employee_staffneeded:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_staffneeded}
                    readOnly="true"
                  />
                  <p className="list">employee_jobtasks:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_jobtasks}
                    readOnly="true"
                  />
                  <p className="list">employee_type_of_staff_needed:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_type_of_staff_needed}
                    readOnly="true"
                  />
                  <p className="list">employee_staffsignedup:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_staffsignedup}
                    readOnly="true"
                  />
                  <p className="list">employee_language:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_language}
                    readOnly="true"
                  />
                  <p className="list">employee_general_notes_to_staff:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_general_notes_to_staff}
                    readOnly="true"
                  />
                  <p className="list">employee_dutymanagers:</p>
                  <input
                    className="resultInLists"
                    value={ele.employee_dutymanagers}
                    readOnly="true"
                  />

                  <button
                    value={idx}
                    onClick={() => handleToggle(idx)}>See less details</button>
                </div>

            }
          </div>
          //  </div>
        })
      }

      {/* ================== */}
    </div>
  )
}

export default EmployeeFind