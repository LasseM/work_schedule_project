import React, { useState } from 'react'
import { url } from '../../../config'
import axios from 'axios'
import EmployeeFindOne from './find_one_employee'
import EmptyFormEmployeeContent from './emptyFormEmployeeContent'

const EmployeeRemove = (props) => {

    const [inputsFromSearch, setInputsFromSearch] = useState({})
    const [form, setForm] = useState(EmptyFormEmployeeContent)

 
    // ====== Removed Employee 
    // const [removedEmployee, setRemovedEmployee] = useState('')

    const input = (res) => {

        console.log("res inside input", res)
        setInputsFromSearch({inputsFromSearch, ...res})


    }
    console.log("inputsFromSearch inside input", inputsFromSearch)

    const handleFormChange = (e) => {

        setForm({...form,[e.target.name]:e.target.value})
        console.log("form after handleFormChange", form)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
    
    // setRemovedEmployee([...removedEmployee, data])
    // console.log('removedEmployee =>', removedEmployee)

    try {
        const response = await axios.delete(`${url}/employees/remove/${inputsFromSearch._id}`)
        console.log('response after try', response)
        alert(`Employee with id: >> ${inputsFromSearch._id} << and name: ${inputsFromSearch.employee_firstname} ${inputsFromSearch.employee_lastname} has been removed from database`)
    }catch(error){
        console.log(error)
    }
}

    return (
        <div>

        <EmployeeFindOne input={input} />

         <form 
         onSubmit={handleSubmit} 
         className="CRUD-form" 
         >
            <h3>Delete an Employee</h3>

            Employee _id:<input name='_id'
                                onChange={handleFormChange}
                                value={inputsFromSearch._id}
                                placeholder={inputsFromSearch._id}/> 
            <button>Remove Employee</button>
        </form>



        </div>
    )
}

export default EmployeeRemove