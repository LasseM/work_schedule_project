const EmptyFormEmployeeContent = {

    _id: '',
    employeeID: '',
    employee_password: '',
    employeeType: '',
    employee_image: {
        photo_url: "",
        public_id: "",
    },
    employee_firstname: '',
    employee_middlename: '',
    employee_lastname: '',
    employee_email: '',
    employee_addressStreet: '',
    employee_addressNumber: '',
    employee_postalCode: '',
    employee_city: '',
    employee_country: '',
    employee_phone: '',
    employee_mobile: '',
    employee_office: '',
    employee_position: '',
    employee_focusarea: '',
    employee_nationality: '',
    employee_language: '',
    employee_bank_reg: '',
    employee_bank_account_number: '',
    employee_payroll_num: '',
    employee_tax_card: '',

}
export default EmptyFormEmployeeContent