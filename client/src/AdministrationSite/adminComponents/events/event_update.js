import React, { useState, useEffect } from 'react'
import emptyFormEventContent from '../events/emptyFormEventContent'
import { url } from '../../../config'
import axios from 'axios'
import EventFindOne from './find_one_event'



const EventUpdate = (props) => {


    const [MyProfileForm, setMyProfileForm] = useState({})

    // const [input, setInput] = useState({})
    console.log("props", props)
    console.log("props.userInfo", props.userInfo)

    useEffect(() => {
        setMyProfileForm({ ...MyProfileForm, ...props.userInfo })

    }, [])

    const [initialInput, setInitialInput] = useState({})
    const [inputsFromSearch, setInputsFromSearch] = useState({})
    const [form, setForm] = useState(emptyFormEventContent)

    //Toggle Render
    //const [toggleRender] = useState(false)




    const input = (res) => {
        console.log("res from input =>", res)
        setInputsFromSearch({ ...inputsFromSearch, ...res })
        setInitialInput({ ...inputsFromSearch, ...res })

    }
    console.log("inputsFromSearch after input =>", inputsFromSearch)

    const handleFormChange = (e) => {
        console.log(e.target.name, e.target.value)
        setInputsFromSearch({ ...inputsFromSearch, [e.target.name]: e.target.value })
    }

    const CancelChange = (res) => {
        setInputsFromSearch({ ...inputsFromSearch, ...initialInput })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        const data = {
            _id: inputsFromSearch._id,
            event_firstname: inputsFromSearch.event_firstname,
            eventID: inputsFromSearch.eventID,
            event_type: inputsFromSearch.event_type,
            event_image: inputsFromSearch.event_image,
            event_logo: inputsFromSearch.event_logo,
            event_middlename: inputsFromSearch.event_middlename,
            event_lastname: inputsFromSearch.event_lastname,
            event_date: inputsFromSearch.event_date,
            event_startdate: inputsFromSearch.event_startdate,
            event_starttime: inputsFromSearch.event_starttime,
            event_enddate: inputsFromSearch.event_enddate,
            event_endtime: inputsFromSearch.event_endtime,
            event_country_location: inputsFromSearch.event_country_location,
            event_city_location: inputsFromSearch.event_city_location,
            event_location: inputsFromSearch.event_location,
            event_adress_street: inputsFromSearch.event_adress_street,
            event_adress_number: inputsFromSearch.event_adress_number,
            event_adress_postcode: inputsFromSearch.event_adress_postcode,
            event_adress_country: inputsFromSearch.event_adress_country,
            event_staffneeded: inputsFromSearch.event_staffneeded,
            event_jobtasks: inputsFromSearch.event_jobtasks,
            event_type_of_staff_needed: inputsFromSearch.event_type_of_staff_needed,
            event_staffsignedup: inputsFromSearch.event_staffsignedup,
            event_language: inputsFromSearch.event_language,
            event_general_notes_to_staff: inputsFromSearch.event_general_notes_to_staff,
            event_dutymanagers: inputsFromSearch.event_dutymanagers,
        }

        console.log('data =>', data)

        if (inputsFromSearch.event_type === '' || inputsFromSearch.event_country_location === '') return alert(`Please fill in all fields`)
        console.log('form before try', form)

        try {
            const response = await axios.put(`${url}/events/update_event`, data)
            console.log(response)
            alert(`Event with id: >> ${inputsFromSearch._id} << and name: ${inputsFromSearch.event_firstname} has been updated`)
        } catch (error) {
            console.log(error)
        }
    }
    return (
        <div >
            <EventFindOne input={input} hideRender={true} />
            <form onSubmit={handleSubmit} className="CRUD-form">
                <h3>Update the event below:</h3>

                <div>* = required fields</div>

                *Event Name: <input
                    required
                    name='event_firstname'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_firstname}
                    placeholder='' />
                EventID:<input name='eventID'
                    onChange={handleFormChange}
                    value={inputsFromSearch.eventID}
                    placeholder='' />
                Event Type:<select onChange={handleFormChange} name='event_type'>
                    <option value="default"> - choose event type - </option>
                    <option value='MICE'>MICE</option>
                    <option value='Cruise'>Cruise</option>
                </select>
                Event Image: <input name='event_image'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_image}
                    placeholder='' />
                Event Logo: <input name='event_logo'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_logo}
                    placeholder='' />
                Event Subname: <input name='event_middlename'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_middlename}
                    placeholder='' />
                Event Lastname: <input name='event_lastname'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_lastname}
                    placeholder='' />
                *Event Date: <input
                    type="date"
                    required
                    name='event_date'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_date}
                />
                Event Startdate: <input name='event_startdate'
                    type="date"
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_startdate}
                    placeholder='' />
                Event Starttime: <input name='event_starttime'
                    type="time"
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_starttime}
                    placeholder='' />
                Event Enddate: <input name='event_enddate'
                    type="date"
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_enddate}
                    placeholder='' />
                Event Endtime: <input name='event_endtime'
                    type="time"
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_endtime}
                    placeholder='' />
                Event Country Location: <select required onChange={handleFormChange} name='event_country_location'>
                    <option value="default">- choose country event city location -</option>
                    <option value="Denmark">Denmark</option>
                    <option value="Sweden">Sweden</option>
                    <option value="Norway">Norway</option>
                    <option value="Estonia">Estonia</option>
                </select>
                Event City Location: <input
                    required
                    name='event_city_location'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_city_location}
                    placeholder='' />
                Event Location: <input name='event_location'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_location}
                    placeholder='' />
                Event Adress Street: <input name='event_adress_street'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_adress_street}
                    placeholder='' />
                Event Adress Number: <input name='event_adress_number'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_adress_number}
                    placeholder='' />
                Event Adress Postcode: <input name='event_adress_postcode'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_adress_postcode}
                    placeholder='' />
                Event Adress Country: <input name='event_adress_country'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_adress_country}
                    placeholder='' />
                Event Staff Needed: <input name='event_staffneeded'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_staffneeded}
                    placeholder='' />
                Event Jobtasks: <input name='event_jobtasks'
                    onChange={handleFormChange}
                    value={inputsFromSearch.even_jobtasks}
                    placeholder='' />
                Event Type Of Staff Needed: <input name='event_type_of_staff_needed'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_type_of_staff_needed}
                    placeholder='' />
                Event Staffsignedup: <input name='event_staffsignedup'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_staffsignedup}
                    placeholder='' />
                Event Language: <input name='event_language'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_language}
                    placeholder='' />
                Event General Notes To Staff: <input name='event_general_notes_to_staff'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_general_notes_to_staff}
                    placeholder='' />
                Event Dutymanagers: <input name='event_dutymanagers'
                    onChange={handleFormChange}
                    value={inputsFromSearch.event_dutymanagers}
                    placeholder='' />
                <button>Update the Event</button>
            </form>

            <button className="cancelChange" onClick={CancelChange}>Cancel Changes</button>
        </div>

    )
}


export default EventUpdate