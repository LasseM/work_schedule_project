import React, { useState, useEffect } from 'react'
import { url } from '../../../config'
import axios from 'axios'

const EventFindOne = (props) => {

  const [form, setForm] = useState('')
  const [findOneEvent, setFindOneEvent] = useState([])
  const [notThere, setNotThere] = useState(false)
  

  
  const handleFormChange = (e) => {
    console.log(e.target.name, e.target.value)
    setForm({...form,[e.target.name]:e.target.value})
}

// ====== Render Toggle ======
const [renderAll, setRenderAll] = useState(true)
  useEffect( () => {
    props.hideRender
    ? setRenderAll(false)
    : setRenderAll(true)
  }, [])


  const addEmployeeID = () => {

    
  }

  const handleSubmit = async (e) => {
    e.preventDefault()

    console.log("form.event_firstname =>", form.event_firstname)
    if(form.event_firstname === undefined && form._id === undefined) return alert(`Please fill in either Event Name or Event System ID before search`);
  
    
    const data = {
      event_firstname: form.event_firstname,
      _id: form._id,
      
    }
    
    try {
      const response = await axios.post(`${url}/events/find_one_event`, data)
      console.log("response", response)
      
      

    if(response.data.ok) {
      let current = findOneEvent
      current =[response.data.response]
      setFindOneEvent([...current])
      props.input(response.data.response)
      setNotThere(false) 
    } else {
      setNotThere(true)
      setFindOneEvent([])
     }
    

  }catch(error){
      console.log(error)
  }
  }
      return (
          <div className="CRUD-form">
            <p>Here you can search for one specific events</p>
            <form onSubmit={handleSubmit} className="onSubmitForm">
            

            <select>
              <option>- Event Type -</option>
              <option>MICE</option>
              <option>Cruise</option>
            </select>
            <br/>
            
            Event Firstname:<input name='event_firstname'
                            onChange={handleFormChange}
                            value={form.event_firstname}
                            placeholder=''/> 
            
            
            Event Object_id:<input name='_id'
                            onChange={handleFormChange}
                            value={form._id}
                            placeholder=''/> 
            
            <button>Search</button>
            </form>

        {
          renderAll
          ? findOneEvent.length !== 0
           ? findOneEvent.map( (ele, idx) =>{
                    
                    console.log('ele from UpdateOneEvent:', ele)
                return  <div key={idx} className="returnedEvents">
      <p className="list">Event:</p>
      <input 
      value={ele._id }
      name="ele._id"
      readOnly="true"
      />
      <p className="list">event_firstname:</p>
      <input 
      value={ele.event_firstname}
      name="ele.event_firstname"
      readOnly="true"
      />
      <p className="list">eventID:</p>
      <input 
      value={ele.eventID}
      name="ele.eventID"
      readOnly="true"
      /> 
      <p className="list">event_type:</p>
      <input 
      value={ele.event_type}
      name="ele.event_type"
      readOnly="true"
      /> 
      <p className="list">event_image:</p>
      <input 
      value={ele.event_image}
      name="ele.event_image"
      readOnly="true"
      /> 
      <p className="list">event_logo:</p>
      <input 
      value={ele.event_logo}
      name="ele.event_logo"
      readOnly="true"
      /> 
      <p className="list">event_middlename:</p>
      <input 
      value={ele.event_middlename}
      name="ele.event_middlename"
      readOnly="true"
      /> 
      <p className="list">event_lastname:</p>
      <input 
      value={ele.event_lastname}
      name="ele.event_lastname"
      readOnly="true"
      /> 
      <p className="list">event_date:</p>
      <input 
      value={ele.event_date}
      name="ele.event_date"
      readOnly="true"
      /> 
      <p className="list">event_startdate:</p>
      <input 
      value={ele.event_startdate}
      name="ele.event_startdate"
      readOnly="true"
      /> 
      <p className="list">event_starttime:</p>
      <input 
      value={ele.event_starttime}
      name="ele.event_starttime"
      readOnly="true"
      /> 
      <p className="list">event_enddate:</p>
      <input 
      value={ele.event_enddate}
      name="ele.event_enddate"
      readOnly="true"
      /> 
      <p className="list">event_endtime:</p>
      <input 
      value={ele.event_endtime}
      name="ele.event_endtime"
      readOnly="true"
      /> 
      <p className="list">event_country_location:</p>
      <input 
      value={ele.event_country_location}
      name="ele.event_country_location"
      readOnly="true"
      /> 
      <p className="list">event_city_location:</p>
      <input 
      value={ele.event_city_location}
      name="ele.event_city_location"
      readOnly="true"
      /> 
      <p className="list">event_location:</p>
      <input 
      value={ele.event_location}
      name="ele.event_location"
      readOnly="true"
      /> 
      <p className="list">event_adress_street:</p>
      <input 
      value={ele.event_adress_street}
      name="ele.event_adress_street"
      readOnly="true"
      /> 
      <p className="list">event_adress_number:</p>
      <input 
      value={ele.event_adress_number}
      name="ele.event_adress_number"
      readOnly="true"
      /> 
      <p className="list">event_adress_postcode:</p>
      <input 
      value={ele.event_adress_postcode}
      name="ele.event_adress_postcode"
      readOnly="true"
      /> 
      <p className="list">event_adress_country:</p>
      <input 
      value={ele.event_adress_country}
      name="ele.event_adress_country"
      readOnly="true"
      /> 
      <p className="list">event_staffneeded:</p>
      <input 
      value={ele.event_staffneeded}
      name="ele.event_staffneeded"
      readOnly="true"
      /> 
      <p className="list">event_jobtasks:</p>
      <input 
      value={ele.event_jobtasks}
      name="ele.event_jobtasks"
      readOnly="true"
      /> 
      <p className="list">event_type_of_staff_needed:</p>
      <input 
      value={ele.event_type_of_staff_needed}
      name="ele.event_type_of_staff_needed"
      readOnly="true"
      /> 
      <p className="list">event_staffsignedup:</p>
      <input 
      value={ele.event_staffsignedup}
      name="ele.event_staffsignedup"
      readOnly="true"
      /> 
      <p className="list">event_language:</p>
      <input 
      value={ele.event_language}
      name="ele.event_language"
      readOnly="true"
      /> 
      <p className="list">event_general_notes_to_staff:</p>
      <input 
      value={ele.event_general_notes_to_staff}
      name="ele.event_general_notes_to_staff"
      readOnly="true"
      /> 
      <p className="list">event_dutymanagers:</p>
      <input 
      value={ele.event_dutymanagers}
      name="ele.event_dutymanagers"
      readOnly="true"
      /> 
                </div>
                  }):<div>Input not recognized</div>
      : null
      }
      

          </div>
      )
}

export default EventFindOne