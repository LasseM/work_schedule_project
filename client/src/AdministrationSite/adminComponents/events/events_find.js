import React, { useState } from 'react'
import { url } from '../../../config'
import axios from 'axios'



const EventFind = () => {


  //==== Get full year from inputfield
  const [inputfullYear, setinputFullYear] = useState('')
 const handleYearChange = (e) => {
   setinputFullYear({...inputfullYear, [e.target.name]:e.target.value})
 }

 const [form, setForm] = useState('')
  //========Find event
  const [findEvent, setFindEvent] = useState([])


 
  const handleFormChange = (e) => {
      // console.log(e.target.name, e.target.value)
      setForm({...form,[e.target.name]:e.target.value})
  }


//====== HandleSubmit ====== 
const handleSubmit = async (e) => {
  e.preventDefault()

try {
    const response = await axios.get(`${url}/events/find_events`)
      const temp = []
      
      console.log("response:", response)

      response.data.response.forEach((ele) => {

        let tempDate = ''
        let tempDateMonth = ''

        console.log("ele. event dates ", ele.event_date)
        tempDate = ele.event_date
        tempDateMonth = ele.event_date

        let tempYear = tempDate.slice(0, 4) 
        console.log("tempYear:", tempYear)

       
        let tempMonth = tempDateMonth.slice(6, 2) 
        console.log("tempMonth:", tempMonth)
      
        console.log("inputfullYear:", inputfullYear.chooseYear)
        
      
        if(ele.event_type === form.event_type && tempYear === inputfullYear.chooseYear){
        temp.push(ele)
        }else if (form.event_type === 'All Event Types' && tempYear === inputfullYear.chooseYear){
          temp.push(ele)
        }
      });
      setFindEvent([...temp])
      console.log('temp:', temp)
}catch(error){
    console.log(error)
}
}
    return (
        <div className="CRUD-form">
          <h3>Here you can search for all events</h3>
          <form onSubmit={handleSubmit}>

      
        
        <div>
          <p>*Choose Year:</p> 
          <input onChange={handleYearChange} 
                  type="string"
                  name="chooseYear"/>
        </div>
        <div>
          <p>*Choose Event Type:</p>                        
          <select onChange={handleFormChange} name='event_type'>
                  <option value="nothing"> - choose event type - </option>
                  <option value="All Event Types"> - All Event Types - </option>
                  <option value='MICE'>MICE</option>
                  <option value='Cruise'>Cruise</option>
          </select>
        </div>
          <button>Search</button>
          </form>

{/* ======= Return of the search ====== */}

{
            findEvent.map( (ele, idx) =>{
           return  <div key={idx} >
<h2>Search results for {form.event_type}:</h2>
<div className="returnedEvents">
<p className="list">Number of search results:</p> <p className="resultInLists">{idx + 1} of {findEvent.length}</p>
<p className="list">event_firstname:</p> 
<input 
className="resultInLists"
value={ele.event_firstname}
readOnly="true"
/>
<p className="list">systemID:</p> 
<input 
className="resultInLists"
value={ele._id}
readOnly="true"
/>
<p className="list">eventID:</p> 
<input 
className="resultInLists"
value={ele.eventID}
readOnly="true"
/> 
<p className="list">event_type:</p> 
<input 
className="resultInLists"
value={ele.event_type}
readOnly="true"
/> 
<p className="list">event_image:</p> 
<input 
className="resultInLists"
value={ele.event_image}
readOnly="true"
/> 
<p className="list">event_logo:</p> 
<input 
className="resultInLists"
value={ele.event_logo}
readOnly="true"
/> 
<p className="list">event_middlename:</p> 
<input 
className="resultInLists"
value={ele.event_middlename}
readOnly="true"
/> 
<p className="list">event_lastname:</p> 
<input 
className="resultInLists"
value={ele.event_lastname}
readOnly="true"
/> 
<p className="list">event_date:</p> 
<input 
className="resultInLists"
value={ele.event_date}
readOnly="true"
/> 
<p className="list">event_startdate:</p> 
<input 
className="resultInLists"
value={ele.event_startdate}
readOnly="true"
/> 
<p className="list">event_starttime:</p> 
<input 
className="resultInLists"
value={ele.event_starttime}
readOnly="true"
/> 
<p className="list">event_enddate:</p> 
<input 
className="resultInLists"
value={ele.event_enddate}
readOnly="true"
/> 
<p className="list">event_endtime:</p> 
<input 
className="resultInLists"
value={ele.event_endtime}
readOnly="true"
/> 
<p className="list">event_country_location:</p> 
<input 
className="resultInLists"
value={ele.event_country_location}
readOnly="true"
/> 
<p className="list">event_city_location:</p> 
<input 
className="resultInLists"
value={ele.event_city_location}
readOnly="true"
/> 
<p className="list">event_location:</p> 
<input 
className="resultInLists"
value={ele.event_location}
readOnly="true"
/> 
<p className="list">event_adress_street:</p> 
<input 
className="resultInLists"
value={ele.event_adress_street}
readOnly="true"
/> 
<p className="list">event_adress_number:</p> 
<input 
className="resultInLists"
value={ele.event_adress_number}
readOnly="true"
/> 
<p className="list">event_adress_postcode:</p> 
<input 
className="resultInLists"
value={ele.event_adress_postcode}
readOnly="true"
/> 
<p className="list">event_adress_country:</p> 
<input 
className="resultInLists"
value={ele.event_adress_country}
readOnly="true"
/> 
<p className="list">event_staffneeded:</p> 
<input 
className="resultInLists"
value={ele.event_staffneeded}
readOnly="true"
/> 
<p className="list">event_jobtasks:</p> 
<input 
className="resultInLists"
value={ele.event_jobtasks}
readOnly="true"
/> 
<p className="list">event_type_of_staff_needed:</p> 
<input 
className="resultInLists"
value={ele.event_type_of_staff_needed}
readOnly="true"
/> 
<p className="list">event_staffsignedup:</p> 
<input 
className="resultInLists"
value={ele.event_staffsignedup}
readOnly="true"
/> 
<p className="list">event_language:</p> 
<input 
className="resultInLists"
value={ele.event_language}
readOnly="true"
/> 
<p className="list">event_general_notes_to_staff:</p> 
<input 
className="resultInLists"
value={ele.event_general_notes_to_staff}
readOnly="true"
/> 
<p className="list">event_dutymanagers:</p> 
<input 
className="resultInLists"
value={ele.event_dutymanagers}
readOnly="true"
/> 
</div>
           </div>
            })
          }

{/* ================== */}
        </div>
    )
}

export default EventFind