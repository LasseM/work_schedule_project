const formEventContent = {

    event_firstname: 'Eventname',
    eventID: '12',
    event_type: '',
    office_image: {
        photo_url: "",
        public_id: "",
    }, 
    office_logo: {
        photo_url: "",
        public_id: "",
    },
    event_middlename: '$$',
    event_lastname: '££',
    event_date: '31.12.2019',
    event_startdate: '31.12.2019',
    event_starttime: '10:00',
    event_enddate: '01.01.2020',
    event_endtime: '€€',
    event_country_location: '',
    event_city_location: 'Horsens',
    event_location: 'Opus Hotel',
    event_adress_street: 'Hovedvej',
    event_adress_number: '10',
    event_adress_postcode: '7800',
    event_adress_country: '',
    event_staffneeded: '50',
    event_jobtasks: ['Meet&Greet'],
    event_type_of_staff_needed: ['Guides'],
    event_staffsignedup: ['Peter'],
    event_language: ['Spanish'],
    event_general_notes_to_staff: 'Get on board',
    event_dutymanagers: ['Steven', 'Peter', 'Jake'],

}
export default formEventContent