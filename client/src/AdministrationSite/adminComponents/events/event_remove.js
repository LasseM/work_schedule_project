import React, { useState } from 'react'
import { url } from '../../../config'
import axios from 'axios'
import EventFindOne from './find_one_event'
import EmptyFormEventContent from '../events/emptyFormEventContent'

const EventRemove = () => {
    const [inputsFromSearch, setInputsFromSearch] = useState({})
    const [form, setForm] = useState(EmptyFormEventContent)

    // ====== Removed Event 
    // const [removedEvent, setRemovedEvent] = useState('')

    const input = (res) => {
        console.log("res from input =>", res)
        setInputsFromSearch({inputsFromSearch, ...res})

    }


    const handleFormChange = (e) => {
        console.log(e.target.name, e.target.value)
        setForm({...form,[e.target.name]:e.target.value})
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
    
    // setRemovedEvent([...removedEvent, data])
    // console.log('removedEvent =>', removedEvent)

    try {
        const response = await axios.delete(`${url}/events/remove/${inputsFromSearch._id}`)
        console.log('response inside try in remove events', response)
        alert(`Event with system id: >> ${inputsFromSearch._id} << and name: ${inputsFromSearch.event_firstname} has been deleted from database`)
    }catch(error){
        console.log(error)
        alert('Something went wrong')
    }
}

    return (
        <div>

        <EventFindOne input={input} />

         <form 
         onSubmit={handleSubmit} 
         className="CRUD-form" 
         >
            <h3>Delete an Event</h3>

            Event _id:<input name='_id'
                                onChange={handleFormChange}
                                value={inputsFromSearch._id}
                                placeholder={inputsFromSearch._id}/> 
            <button>Remove Event</button>
        </form>



        </div>
    )
}

export default EventRemove