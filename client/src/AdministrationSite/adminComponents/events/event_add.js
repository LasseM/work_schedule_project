import React, { useState, useEffect } from 'react'
import formEventContent from './formEventContent'
import { url } from '../../../config'
import axios from 'axios'

import DropDownEmployees from '../employees/dropDownEmployees'

const EventAdd = (props) => {


    // ====== chosenEmployees ====== //

    // const chosenEmployeesList = []
    const [chosenEmployeesList, setchosenEmployeesList] = useState([])

    // const [EmpListShowing, setEmpListShowing] = useState([])

    // useEffect(() => {
    //     if (isFreelancerThere) {
    //         setEmpListShowing([chosenEmployeesList])
    //     }
    //     console.log('EmpListShowing ==>', EmpListShowing)
    // }, [isFreelancerThere])



    const chosenEmployees = (item, idx) => {
  
const index = chosenEmployeesList.findIndex(ele=> ele.employee_firstname === item.employee_firstname)
        if (index === -1) {
 
            setchosenEmployeesList([...chosenEmployeesList, item])
        }
        console.log('chosenEmployeesList ==>', chosenEmployeesList)
        
    }

    const [form, setForm] = useState(formEventContent)

    const handleFormChange = (e) => {
        console.log(e.target.name, e.target.value)
        setForm({ ...form, [e.target.name]: e.target.value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        const data = {
            event_firstname: form.event_firstname,
            eventID: form.eventID,
            event_type: form.event_type,
            event_image: form.event_image,
            event_logo: form.event_logo,
            event_middlename: form.event_middlename,
            event_lastname: form.event_lastname,
            event_date: form.event_date,
            event_startdate: form.event_startdate,
            event_starttime: form.event_starttime,
            event_enddate: form.event_enddate,
            event_endtime: form.event_endtime,
            event_country_location: form.event_country_location,
            event_city_location: form.event_city_location,
            event_location: form.event_location,
            event_adress_street: form.event_adress_street,
            event_adress_number: form.event_adress_number,
            event_adress_postcode: form.event_adress_postcode,
            event_adress_country: form.event_adress_country,
            event_staffneeded: form.event_staffneeded,
            // event_jobtasks: [],
            event_type_of_staff_needed: form.event_type_of_staff_needed,
            event_staffsignedup: chosenEmployeesList,
            // event_language: [],
            event_general_notes_to_staff: form.event_general_notes_to_staff,
            // event_dutymanagers: [],
        }

        data['event_jobtasks'].push(form.event_jobtasks)
        // data['event_staffsignedup'].push(form.event_staffsignedup)
        data['event_language'].push(form.event_language)

        data['event_dutymanagers'].push(form.event_dutymanagers)




        console.log('data =>', data)

        if (form.event_type === undefined || form.event_country_location === undefined) return alert(`Please fill in all fields`)
        console.log('form before try', form)

        try {
            const response = await axios.post(`${url}/events/add`, data)
            console.log('response after try', response)

            let tempMsg = response.data.message.slice(0, 6)
            if (response.data.ok === false && tempMsg === 'E11000') {

                return alert(`Error! EventID has to be unique, an event with EventID:${form.eventID} already exists in the database.`)
            }
            else if (response.data.ok === true) {

                return alert(`Event with EventID: ${form.eventID} and name: ${form.event_firstname} has been created in the database.`)
            }


        } catch (error) {
            console.log(error)


        }
    }
    return (
        <div >
            <form onSubmit={handleSubmit} className="CRUD-form" >
                <h3>Adding a new event</h3>
                <div>* = required fields</div>

                *Event Name: <input
                    required
                    name='event_firstname'
                    onChange={handleFormChange}
                    value={form.event_firstname}
                    placeholder='' />
                EventID:<input name='eventID'
                    onChange={handleFormChange}
                    value={form.eventID}
                    placeholder='' />
                Event Type:<select onChange={handleFormChange} name='event_type'>
                    <option value="default"> - choose event type - </option>
                    <option value='MICE'>MICE</option>
                    <option value='Cruise'>Cruise</option>
                </select>
                Event Image: <input name='event_image'
                    onChange={handleFormChange}
                    value={form.event_image}
                    placeholder='' />
                Event Logo: <input name='event_logo'
                    onChange={handleFormChange}
                    value={form.event_logo}
                    placeholder='' />
                Event Subname: <input name='event_middlename'
                    onChange={handleFormChange}
                    value={form.event_middlename}
                    placeholder='' />
                Event Lastname: <input name='event_lastname'
                    onChange={handleFormChange}
                    value={form.event_lastname}
                    placeholder='' />
                *Event Date: <input
                    type="date"
                    required
                    name='event_date'
                    onChange={handleFormChange}
                    value={form.event_date}
                />
                Event Startdate: <input name='event_startdate'
                    type="date"
                    onChange={handleFormChange}
                    value={form.event_startdate}
                    placeholder='' />
                Event Starttime: <input name='event_starttime'
                    type="time"
                    onChange={handleFormChange}
                    value={form.event_starttime}
                    placeholder='' />
                Event Enddate: <input name='event_enddate'
                    type="date"
                    onChange={handleFormChange}
                    value={form.event_enddate}
                    placeholder='' />
                Event Endtime: <input name='event_endtime'
                    type="time"
                    onChange={handleFormChange}
                    value={form.event_endtime}
                    placeholder='' />
                Event Country Location: <select required onChange={handleFormChange} name='event_country_location'>
                    <option value="default">- choose country event city location -</option>
                    <option value="Denmark">Denmark</option>
                    <option value="Sweden">Sweden</option>
                    <option value="Norway">Norway</option>
                    <option value="Estonia">Estonia</option>
                </select>
                Event City Location: <input
                    required
                    name='event_city_location'
                    onChange={handleFormChange}
                    value={form.event_city_location}
                    placeholder='' />
                Event Location: <input name='event_location'
                    onChange={handleFormChange}
                    value={form.event_location}
                    placeholder='' />
                Event Adress Street: <input name='event_adress_street'
                    onChange={handleFormChange}
                    value={form.event_adress_street}
                    placeholder='' />
                Event Adress Number: <input name='event_adress_number'
                    onChange={handleFormChange}
                    value={form.event_adress_number}
                    placeholder='' />
                Event Adress Postcode: <input name='event_adress_postcode'
                    onChange={handleFormChange}
                    value={form.event_adress_postcode}
                    placeholder='' />
                Event Adress Country: <input name='event_adress_country'
                    onChange={handleFormChange}
                    value={form.event_adress_country}
                    placeholder='' />
                Event Staff Needed: <input name='event_staffneeded'
                    onChange={handleFormChange}
                    value={form.event_staffneeded}
                    placeholder='' />
                Event Jobtasks: <input name='event_jobtasks'
                    onChange={handleFormChange}
                    value={form.even_jobtasks}
                    placeholder='' />
                Event Type Of Staff Needed: <input name='event_type_of_staff_needed'
                    onChange={handleFormChange}
                    value={form.event_type_of_staff_needed}
                    placeholder='' />
                <DropDownEmployees chosenEmployees={chosenEmployees} />
                Event Staffsignedup:
                {/* <input name='event_staffsignedup'
                    onChange={handleFormChange}
                    value={chosenEmployeesList}
                    placeholder='' /> */}

                <select>
                    {
                        (chosenEmployeesList !== undefined && chosenEmployeesList.length > 0)
                            ?
                            chosenEmployeesList.map((item, idx) => {
                                // debugger
                                return <option className="chosenEmployees" value={idx}>{item.employee_firstname} {item.employee_lastname}</option>
                            })
                            : null
                    }
                </select>



                Event Language: <input name='event_language'
                    onChange={handleFormChange}
                    value={form.event_language}
                    placeholder='' />
                Event General Notes To Staff: <input name='event_general_notes_to_staff'
                    onChange={handleFormChange}
                    value={form.event_general_notes_to_staff}
                    placeholder='' />
                Event Dutymanagers: <input name='event_dutymanagers'
                    onChange={handleFormChange}
                    value={form.event_dutymanagers}
                    placeholder='' />
                <button>Add new Event</button>
            </form>






        </div>
    )
}

export default EventAdd