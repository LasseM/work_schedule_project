const EmptyFormEventContent = {

    _id: '',
    event_firstname: '',
    eventID: '',
    event_type: 'something',
    office_image: {
        photo_url: "",
        public_id: "",
    }, 
    office_logo: {
        photo_url: "",
        public_id: "",
    },
    event_middlename: '',
    event_lastname: '',
    event_date: '',
    event_startdate: '',
    event_starttime: '',
    event_enddate: '',
    event_endtime: '',
    event_country_location: 'something',
    event_city_location: '',
    event_location: '',
    event_adress_street: '',
    event_adress_number: '',
    event_adress_postcode: '',
    event_adress_country: '',
    event_staffneeded: '',
    event_jobtasks: [],
    event_type_of_staff_needed: [],
    event_staffsignedup: [],
    event_language: [],
    event_general_notes_to_staff: '',
    event_dutymanagers: [],    
    

}
export default EmptyFormEventContent