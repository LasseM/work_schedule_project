import React, { useState } from 'react'
import { url } from '../../../config'
import axios from 'axios'

const EventFindOne = (props) => {

  const [form, setForm] = useState('')
  const [findOneEvent, setFindOneEvent] = useState([])
  const [notThere, setNotThere] = useState(false)
  
  const handleFormChange = (e) => {
    console.log(e.target.name, e.target.value)
    setForm({...form,[e.target.name]:e.target.value})
}

  const handleSubmit = async (e) => {
    e.preventDefault()
  
    const data = {
      event_firstname: form.event_firstname,
      _id: form._id,
      
    }
    

  try {
      const response = await axios.post(`${url}/events/find_one_event`, data)
      console.log("response", response)

      

    if(response.data.ok) {
      let current = findOneEvent
      current =[response.data.response]
      setFindOneEvent([...current])
      props.input_id(response.data.response._id)
      setNotThere(false) 
    } else {
      setNotThere(true)
      setFindOneEvent([])
     }
    

  }catch(error){
      console.log(error)
  }
  }
      return (
          <div className="CRUD-form">
            <p>Here you can search for one specific events</p>
            <form onSubmit={handleSubmit} className="onSubmitForm">
            

            <select>
              <option>- choose Event Type -</option>
              <option>MICE</option>
              <option>Cruise</option>
            </select>
            <br/>
            
            Event Firstname:<input name='event_firstname'
                            onChange={handleFormChange}
                            value={form.event_firstname}
                            placeholder=''/> 
            
            
            Event Object_id:<input name='_id'
                            onChange={handleFormChange}
                            value={form._id}
                            placeholder=''/> 
            
            <button>Search</button>
            </form>


      { findOneEvent.length !== 0
      ? findOneEvent.map( (ele, idx) =>{
                    
                    console.log('ele from UpdateOneEvent:', ele)
                return  <div key={idx} className="returnedEvents">
      <p>Event: {ele._id }</p>
      <p>event_firstname: {ele.event_firstname}</p>
      <p>eventID: {ele.eventID}</p> 
      <p>event_type: {ele.event_type}</p> 
      <p>event_image: {ele.event_image}</p> 
      <p>event_logo: {ele.event_logo}</p> 
      <p>event_middlename: {ele.event_middlename}</p> 
      <p>event_lastname: {ele.event_lastname}</p> 
      <p>event_date: {ele.event_date}</p> 
      <p>event_startdate: {ele.event_startdate}</p> 
      <p>event_starttime: {ele.event_starttime}</p> 
      <p>event_enddate: {ele.event_enddate}</p> 
      <p>event_endtime: {ele.event_endtime}</p> 
      <p>event_country_location: {ele.event_country_location}</p> 
      <p>event_city_location: {ele.event_city_location}</p> 
      <p>event_location: {ele.event_location}</p> 
      <p>event_adress_street: {ele.event_adress_street}</p> 
      <p>event_adress_number: {ele.event_adress_number}</p> 
      <p>event_adress_postcode: {ele.event_adress_postcode}</p> 
      <p>event_adress_country: {ele.event_adress_country}</p> 
      <p>event_staffneeded: {ele.event_staffneeded}</p> 
      <p>event_jobtasks: {ele.event_jobtasks}</p> 
      <p>event_type_of_staff_needed: {ele.event_type_of_staff_needed}</p> 
      <p>event_staffsignedup: {ele.event_staffsignedup}</p> 
      <p>event_language: {ele.event_language}</p> 
      <p>event_general_notes_to_staff: {ele.event_general_notes_to_staff}</p> 
      <p>event_dutymanagers: {ele.event_dutymanagers}</p> 
                </div>
                  }):<div>Input not recognized</div>
      }
      

          </div>
      )
}

export default EventFindOne