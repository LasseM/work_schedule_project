import React, { useState } from 'react'
import { url } from '../../../config'
import axios from 'axios'
import JobtaskFindOne from './find_one_jobtask'
import EmptyFormJobtaskContent from '../jobtasks/emptyFormJobtaskContent'

const JobtaskRemove = () => {
    const [inputsFromSearch, setInputsFromSearch] = useState({})
    const [form, setForm] = useState(EmptyFormJobtaskContent)

    // ====== Removed jobtask 
    // const [removedjobtask, setRemovedjobtask] = useState('')

    const input = (res) => {
        console.log("res from input =>", res)
        setInputsFromSearch({inputsFromSearch, ...res})

    }


    const handleFormChange = (e) => {
        console.log(e.target.name, e.target.value)
        setForm({...form,[e.target.name]:e.target.value})
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
    
    // setRemovedjobtask([...removedjobtask, data])
    // console.log('removedjobtask =>', removedjobtask)

    try {
        const response = await axios.delete(`${url}/jobtasks/remove/${inputsFromSearch._id}`)
        console.log('response inside try in remove jobtasks', response)
        alert(`jobtask with system id: >> ${inputsFromSearch._id} << and name: ${inputsFromSearch.jobtask_firstname} has been deleted from database`)
    }catch(error){
        console.log(error)
        alert('Something went wrong')
    }
}

    return (
        <div>

        <JobtaskFindOne input={input} />

         <form 
         onSubmit={handleSubmit} 
         className="CRUD-form" 
         >
            <h3>Delete an jobtask</h3>

            jobtask _id:<input name='_id'
                                onChange={handleFormChange}
                                value={inputsFromSearch._id}
                                placeholder={inputsFromSearch._id}/> 
            <button>Remove jobtask</button>
        </form>



        </div>
    )
}

export default JobtaskRemove