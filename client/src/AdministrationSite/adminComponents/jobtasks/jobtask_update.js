import React, { useState } from 'react'
import emptyFormJobtaskContent from '../jobtasks/emptyFormJobtaskContent'
import { url } from '../../../config'
import axios from 'axios'
import JobtaskFindOne from './find_one_jobtask'




const JobtaskUpdate = () => {
    const [initialInput, setInitialInput] = useState({})
    const [inputsFromSearch, setInputsFromSearch] = useState({})
    const [form, setForm] = useState(emptyFormJobtaskContent)
    

    const input = (res) => {
        console.log("res from input =>", res)
        setInputsFromSearch({inputsFromSearch, ...res})
        setInitialInput({...inputsFromSearch, ...res})

    }
    console.log("inputsFromSearch after input =>", inputsFromSearch)

    const handleFormChange = (e) => {
        console.log(e.target.name, e.target.value)
        setInputsFromSearch({...inputsFromSearch,[e.target.name]:e.target.value})
    }

    const CancelChange = (res) => {
        setInputsFromSearch({...inputsFromSearch, ...initialInput})
      }
    
    const handleSubmit = async (e) => {
        e.preventDefault()

        const data = {
            _id: inputsFromSearch._id,
            jobtask_firstname: inputsFromSearch.jobtask_firstname,
            jobtaskID: inputsFromSearch.jobtaskID,
            jobtask_type: inputsFromSearch.jobtask_type,
            jobtask_image: inputsFromSearch.jobtask_image,
            jobtask_logo: inputsFromSearch.jobtask_logo,
            jobtask_middlename: inputsFromSearch.jobtask_middlename,
            jobtask_lastname: inputsFromSearch.jobtask_lastname,
            jobtask_date: inputsFromSearch.jobtask_date,
            jobtask_startdate: inputsFromSearch.jobtask_startdate,
            jobtask_starttime: inputsFromSearch.jobtask_starttime,
            jobtask_enddate: inputsFromSearch.jobtask_enddate,
            jobtask_endtime: inputsFromSearch.jobtask_endtime,
            jobtask_country_location: inputsFromSearch.jobtask_country_location,
            jobtask_city_location: inputsFromSearch.jobtask_city_location,
            jobtask_location: inputsFromSearch.jobtask_location,
            jobtask_adress_street: inputsFromSearch.jobtask_adress_street,
            jobtask_adress_number: inputsFromSearch.jobtask_adress_number,
            jobtask_adress_postcode: inputsFromSearch.jobtask_adress_postcode,
            jobtask_adress_country: inputsFromSearch.jobtask_adress_country,
            jobtask_staffneeded: inputsFromSearch.jobtask_staffneeded,
            jobtask_jobtasks: inputsFromSearch.jobtask_jobtasks,
            jobtask_type_of_staff_needed: inputsFromSearch.jobtask_type_of_staff_needed,
            jobtask_staffsignedup: inputsFromSearch.jobtask_staffsignedup,
            jobtask_language: inputsFromSearch.jobtask_language,
            jobtask_general_notes_to_staff: inputsFromSearch.jobtask_general_notes_to_staff,
            jobtask_dutymanagers: inputsFromSearch.jobtask_dutymanagers,
        }
        
        console.log('data =>', data)

        if(inputsFromSearch.jobtask_type === '' || inputsFromSearch.jobtask_country_location === '') return alert(`Please fill in all fields`)
        console.log('form before try', form)
        
        try {
            const response = await axios.put(`${url}/jobtasks/update_jobtask`, data)
            console.log(response)
            alert(`jobtask with id: >> ${inputsFromSearch._id} << and name: ${inputsFromSearch.jobtask_firstname} has been updated`)
        }catch(error){
            console.log(error)
        }
    }
    return (
        <div >
                    <JobtaskFindOne input={input} hideRender={false}/>
                    
                    
            <form onSubmit={handleSubmit} className="CRUD-form">
                <h3>Update the jobtask below:</h3>
        
                <div>* = required fields</div>

                *jobtask Name: <input 
                                    required
                                    name='jobtask_firstname'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_firstname}
                                    placeholder=''/>
                jobtaskID:<input name='jobtaskID'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtaskID}
                                    placeholder=''/> 
                jobtask Type:<select onChange={handleFormChange} name='jobtask_type'>
                        <option value="default"> - choose jobtask type - </option>
                        <option value='MICE'>MICE</option>
                        <option value='Cruise'>Cruise</option>
                        </select>
                jobtask Image: <input name='jobtask_image'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_image}
                                    placeholder=''/>
                jobtask Logo: <input name='jobtask_logo'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_logo}
                                    placeholder=''/>
                jobtask Subname: <input name='jobtask_middlename'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_middlename}
                                    placeholder=''/>
                jobtask Lastname: <input name='jobtask_lastname'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_lastname}
                                    placeholder=''/>
                *jobtask Date: <input 
                                    type="date"
                                    required
                                    name='jobtask_date'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_date}
                                    />
                jobtask Startdate: <input name='jobtask_startdate'
                                    type="date"
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_startdate}
                                    placeholder=''/>
                jobtask Starttime: <input name='jobtask_starttime'
                                    type="time"
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_starttime}
                                    placeholder=''/>
                jobtask Enddate: <input name='jobtask_enddate'
                                    type="date"
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_enddate}
                                    placeholder=''/>
                jobtask Endtime: <input name='jobtask_endtime'
                                    type="time"
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_endtime}
                                    placeholder=''/>
                jobtask Country Location: <select required onChange={handleFormChange} name='jobtask_country_location'>
                        <option value="default">- choose country jobtask city location -</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Norway">Norway</option>
                        <option value="Estonia">Estonia</option>
                        </select>
                jobtask City Location: <input 
                                    required
                                    name='jobtask_city_location'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_city_location}
                                    placeholder=''/>
                jobtask Location: <input name='jobtask_location'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_location}
                                    placeholder=''/>
                jobtask Adress Street: <input name='jobtask_adress_street'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_adress_street}
                                    placeholder=''/>
                jobtask Adress Number: <input name='jobtask_adress_number'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_adress_number}
                                    placeholder=''/>
                jobtask Adress Postcode: <input name='jobtask_adress_postcode'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_adress_postcode}
                                    placeholder=''/>
                jobtask Adress Country: <input name='jobtask_adress_country'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_adress_country}
                                    placeholder=''/>
                jobtask Staff Needed: <input name='jobtask_staffneeded'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_staffneeded}
                                    placeholder=''/>
                jobtask Jobtasks: <input name='jobtask_jobtasks'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.even_jobtasks}
                                    placeholder=''/>
                jobtask Type Of Staff Needed: <input name='jobtask_type_of_staff_needed'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_type_of_staff_needed}
                                    placeholder=''/>
                jobtask Staffsignedup: <input name='jobtask_staffsignedup'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_staffsignedup}
                                    placeholder=''/>
                jobtask Language: <input name='jobtask_language'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_language}
                                    placeholder=''/>
                jobtask General Notes To Staff: <input name='jobtask_general_notes_to_staff'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_general_notes_to_staff}
                                    placeholder=''/>
                jobtask Dutymanagers: <input name='jobtask_dutymanagers'
                                    onChange={handleFormChange}
                                    value={inputsFromSearch.jobtask_dutymanagers}
                                    placeholder=''/>
            <button>Update the jobtask</button>
            </form>

            <button className="cancelChange" onClick={CancelChange}>Cancel Changes</button>
        </div>

    )
}


export default JobtaskUpdate