import React, { useState, useEffect } from 'react'
import { url } from '../../../config'
import axios from 'axios'

const JobtaskFindOne = (props) => {

  const [form, setForm] = useState('')
  const [findOneJobtask, setFindOneJobtask] = useState([])
  const [notThere, setNotThere] = useState(false)
  
  const [renderAll, setRenderAll] = useState(true)
  useEffect( () => {
    props.hideRender
    ? setRenderAll(false)
    : setRenderAll(true)
  }, [])

  const handleFormChange = (e) => {
    console.log(e.target.name, e.target.value)
    setForm({...form,[e.target.name]:e.target.value})
  }

  const handleSubmit = async (e) => {
    e.preventDefault()

    console.log("form.jobtask_firstname =>", form.jobtask_firstname)
    if(form.jobtask_firstname === undefined && form._id === undefined) return alert(`Please fill in either jobtask Name or jobtask System ID before search`);
  
    
    const data = {
      jobtask_firstname: form.jobtask_firstname,
      _id: form._id,
      
    }
    
    try {
      const response = await axios.post(`${url}/jobtasks/find_one_jobtask`, data)
      console.log("response", response)
      
      

    if(response.data.ok) {
      let current = findOneJobtask
      current =[response.data.response]
      setFindOneJobtask([...current])
      props.input(response.data.response)
      setNotThere(false) 
    } else {
      setNotThere(true)
      setFindOneJobtask([])
     }
    

  }catch(error){
      console.log(error)
  }
  }
      return (
          <div className="CRUD-form">
            <p>Here you can search for one specific jobtasks</p>
            <form onSubmit={handleSubmit} className="onSubmitForm">
            

            <select>
              <option>- jobtask Type -</option>
              <option>MICE</option>
              <option>Cruise</option>
            </select>
            <br/>
            
            jobtask Firstname:<input name='jobtask_firstname'
                            onChange={handleFormChange}
                            value={form.jobtask_firstname}
                            placeholder=''/> 
            
            
            jobtask Object_id:<input name='_id'
                            onChange={handleFormChange}
                            value={form._id}
                            placeholder=''/> 
            
            <button>Search</button>
            </form>


      {
        renderAll
        ? findOneJobtask.length !== 0
          ? findOneJobtask.map( (ele, idx) =>{
                    
                    
                    console.log('ele from UpdateOnejobtask:', ele)
                return  <div key={idx} className="returnedjobtasks">
      <p className="list">jobtask: {ele._id }</p>
      <p className="list">jobtask_firstname: {ele.jobtask_firstname}</p>
      <p className="list">jobtaskID: {ele.jobtaskID}</p> 
      <p className="list">jobtask_type: {ele.jobtask_type}</p> 
      <p className="list">jobtask_image: {ele.jobtask_image}</p> 
      <p className="list">jobtask_logo: {ele.jobtask_logo}</p> 
      <p className="list">jobtask_middlename: {ele.jobtask_middlename}</p> 
      <p className="list">jobtask_lastname: {ele.jobtask_lastname}</p> 
      <p className="list">jobtask_date: {ele.jobtask_date}</p> 
      <p className="list">jobtask_startdate: {ele.jobtask_startdate}</p> 
      <p className="list">jobtask_starttime: {ele.jobtask_starttime}</p> 
      <p className="list">jobtask_enddate: {ele.jobtask_enddate}</p> 
      <p className="list">jobtask_endtime: {ele.jobtask_endtime}</p> 
      <p className="list">jobtask_country_location: {ele.jobtask_country_location}</p> 
      <p className="list">jobtask_city_location: {ele.jobtask_city_location}</p> 
      <p className="list">jobtask_location: {ele.jobtask_location}</p> 
      <p className="list">jobtask_adress_street: {ele.jobtask_adress_street}</p> 
      <p className="list">jobtask_adress_number: {ele.jobtask_adress_number}</p> 
      <p className="list">jobtask_adress_postcode: {ele.jobtask_adress_postcode}</p> 
      <p className="list">jobtask_adress_country: {ele.jobtask_adress_country}</p> 
      <p className="list">jobtask_staffneeded: {ele.jobtask_staffneeded}</p> 
      <p className="list">jobtask_jobtasks: {ele.jobtask_jobtasks}</p> 
      <p className="list">jobtask_type_of_staff_needed: {ele.jobtask_type_of_staff_needed}</p> 
      <p className="list">jobtask_staffsignedup: {ele.jobtask_staffsignedup}</p> 
      <p className="list">jobtask_language: {ele.jobtask_language}</p> 
      <p className="list">jobtask_general_notes_to_staff: {ele.jobtask_general_notes_to_staff}</p> 
      <p className="list">jobtask_dutymanagers: {ele.jobtask_dutymanagers}</p> 
                </div>
                  }):<div>Input not recognized</div>
      : null
      }
      

          </div>
      )
}

export default JobtaskFindOne