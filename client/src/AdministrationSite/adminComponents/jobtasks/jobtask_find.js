import React, { useState } from 'react'
import { url } from '../../../config'
import axios from 'axios'



const JobtaskFind = () => {


  //==== Get full year from inputfield
  const [inputfullYear, setinputFullYear] = useState('')
 const handleYearChange = (e) => {
   setinputFullYear({...inputfullYear, [e.target.name]:e.target.value})
 }

 const [form, setForm] = useState('')
  //========Find jobtask
  const [findjobtask, setFindjobtask] = useState([])


 
  const handleFormChange = (e) => {
      // console.log(e.target.name, e.target.value)
      setForm({...form,[e.target.name]:e.target.value})
  }


//====== HandleSubmit ====== 
const handleSubmit = async (e) => {
  e.preventDefault()

try {
    const response = await axios.get(`${url}/jobtasks/find_jobtasks`)
      const temp = []
      
      console.log("response:", response)

      response.data.response.forEach((ele) => {

        let tempDate = ''
        let tempDateMonth = ''

        console.log("ele. jobtask dates ", ele.jobtask_date)
        tempDate = ele.jobtask_date
        tempDateMonth = ele.jobtask_date

        let tempYear = tempDate.slice(0, 4) 
        console.log("tempYear:", tempYear)

       
        let tempMonth = tempDateMonth.slice(6, 2) 
        console.log("tempMonth:", tempMonth)
      
        console.log("inputfullYear:", inputfullYear.chooseYear)
        
      
        if(ele.jobtask_type === form.jobtask_type && tempYear === inputfullYear.chooseYear){
        temp.push(ele)
        }else if (form.jobtask_type === 'All jobtask Types' && tempYear === inputfullYear.chooseYear){
          temp.push(ele)
        }
      });
      setFindjobtask([...temp])
      console.log('temp:', temp)
}catch(error){
    console.log(error)
}
}
    return (
        <div className="CRUD-form">
          <h3>Here you can search for all jobtasks</h3>
          <form onSubmit={handleSubmit}>

      
        
        <div>
          <p>*Choose Year:</p> 
          <input onChange={handleYearChange} 
                  type="string"
                  name="chooseYear"/>
        </div>
        <div>
          <p>*Choose jobtask Type:</p>                        
          <select onChange={handleFormChange} name='jobtask_type'>
                  <option value="nothing"> - choose jobtask type - </option>
                  <option value="All jobtask Types"> - All jobtask Types - </option>
                  <option value='MICE'>MICE</option>
                  <option value='Cruise'>Cruise</option>
          </select>
        </div>
          <button>Search</button>
          </form>

{/* ======= Return of the search ====== */}

{
            findjobtask.map( (ele, idx) =>{
           return  <div key={idx} >
<h2>Search results for {form.jobtask_type}:</h2>
<div className="returnedjobtasks">
<p className="list">Number of search results:</p> <p className="resultInLists">{idx + 1} of {findjobtask.length}</p>
<p className="list">jobtask_firstname:</p> <p className="resultInLists">{ele.jobtask_firstname}</p>
<p className="list">systemID:</p> <p className="resultInLists">{ele._id}</p>
<p className="list">jobtaskID:</p> <p className="resultInLists">{ele.jobtaskID}</p> 
<p className="list">jobtask_type:</p> <p className="resultInLists">{ele.jobtask_type}</p> 
<p className="list">jobtask_image:</p> <p className="resultInLists">{ele.jobtask_image}</p> 
<p className="list">jobtask_logo:</p> <p className="resultInLists">{ele.jobtask_logo}</p> 
<p className="list">jobtask_middlename:</p> <p className="resultInLists">{ele.jobtask_middlename}</p> 
<p className="list">jobtask_lastname:</p> <p className="resultInLists">{ele.jobtask_lastname}</p> 
<p className="list">jobtask_date:</p> <p className="resultInLists">{ele.jobtask_date}</p> 
<p className="list">jobtask_startdate:</p> <p className="resultInLists">{ele.jobtask_startdate}</p> 
<p className="list">jobtask_starttime:</p> <p className="resultInLists">{ele.jobtask_starttime}</p> 
<p className="list">jobtask_enddate:</p> <p className="resultInLists">{ele.jobtask_enddate}</p> 
<p className="list">jobtask_endtime:</p> <p className="resultInLists">{ele.jobtask_endtime}</p> 
<p className="list">jobtask_country_location:</p> <p className="resultInLists">{ele.jobtask_country_location}</p> 
<p className="list">jobtask_city_location:</p> <p className="resultInLists">{ele.jobtask_city_location}</p> 
<p className="list">jobtask_location:</p> <p className="resultInLists">{ele.jobtask_location}</p> 
<p className="list">jobtask_adress_street:</p> <p className="resultInLists">{ele.jobtask_adress_street}</p> 
<p className="list">jobtask_adress_number:</p> <p className="resultInLists">{ele.jobtask_adress_number}</p> 
<p className="list">jobtask_adress_postcode:</p> <p className="resultInLists">{ele.jobtask_adress_postcode}</p> 
<p className="list">jobtask_adress_country:</p> <p className="resultInLists">{ele.jobtask_adress_country}</p> 
<p className="list">jobtask_staffneeded:</p> <p className="resultInLists">{ele.jobtask_staffneeded}</p> 
<p className="list">jobtask_jobtasks:</p> <p className="resultInLists">{ele.jobtask_jobtasks}</p> 
<p className="list">jobtask_type_of_staff_needed:</p> <p className="resultInLists">{ele.jobtask_type_of_staff_needed}</p> 
<p className="list">jobtask_staffsignedup:</p> <p className="resultInLists">{ele.jobtask_staffsignedup}</p> 
<p className="list">jobtask_language:</p> <p className="resultInLists">{ele.jobtask_language}</p> 
<p className="list">jobtask_general_notes_to_staff:</p> <p className="resultInLists">{ele.jobtask_general_notes_to_staff}</p> 
<p className="list">jobtask_dutymanagers:</p> <p className="resultInLists">{ele.jobtask_dutymanagers}</p> 
</div>
           </div>
            })
          }

{/* ================== */}
        </div>
    )
}

export default JobtaskFind