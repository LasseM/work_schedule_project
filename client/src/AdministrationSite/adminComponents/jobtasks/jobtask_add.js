import React, { useState } from 'react'
import EmptyFormJobtaskContent from './emptyFormJobtaskContent'
import { url } from '../../../config'
import axios from 'axios'

import EventFindOne from '../events/find_one_event'



const JobtaskAdd = (props) => {

    const [initialInput, setInitialInput] = useState({})
    const [inputsFromSearch, setInputsFromSearch] = useState({})
    
    const [form, setForm] = useState(EmptyFormJobtaskContent)

    const input = (res) => {
        console.log("res from input =>", res)
        setInputsFromSearch({inputsFromSearch, ...res})
        setInitialInput({...inputsFromSearch, ...res})

    }
    console.log("inputsFromSearch after input =>", inputsFromSearch)

    const handleEventFormChange = (e) => {
        console.log(e.target.name, e.target.value)
        setInputsFromSearch({...inputsFromSearch,[e.target.name]:e.target.value})
    }

    const CancelChange = (res) => {
        setInputsFromSearch({...inputsFromSearch, ...initialInput})
      }
    
    
    
    const handleFormChange = (e) => {
        console.log(e.target.name, e.target.value)
        setForm({...form,[e.target.name]:e.target.value})
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        
        const data = {
            jobtask_firstname: form.jobtask_firstname,
            jobtaskID: form.jobtaskID,
            jobtask_type: form.jobtask_type,
            jobtask_image: form.jobtask_image,
            jobtask_logo: form.jobtask_logo,
            jobtask_middlename: form.jobtask_middlename,
            jobtask_lastname: form.jobtask_lastname,
            jobtask_date: form.jobtask_date,
            jobtask_startdate: form.jobtask_startdate,
            jobtask_starttime: form.jobtask_starttime,
            jobtask_enddate: form.jobtask_enddate,
            jobtask_endtime: form.jobtask_endtime,
            jobtask_country_location: form.jobtask_country_location,
            jobtask_city_location: form.jobtask_city_location,
            jobtask_location: form.jobtask_location,
            jobtask_adress_street: form.jobtask_adress_street,
            jobtask_adress_number: form.jobtask_adress_number,
            jobtask_adress_postcode: form.jobtask_adress_postcode,
            jobtask_adress_country: form.jobtask_adress_country,
            jobtask_staffneeded: form.jobtask_staffneeded,
            jobtask_jobtasks: form.jobtask_jobtasks,
            jobtask_type_of_staff_needed: form.jobtask_type_of_staff_needed,
            jobtask_staffsignedup: form.jobtask_staffsignedup,
            jobtask_language: form.jobtask_language,
            jobtask_general_notes_to_staff: form.jobtask_general_notes_to_staff,
            jobtask_dutymanagers: form.jobtask_dutymanagers,
        }

        console.log('data =>',data)

        if(form.jobtask_type === undefined || form.jobtask_country_location === undefined) return alert(`Please fill in all fields`)
        console.log('form before try', form)
        
        try {
            const response = await axios.post(`${url}/jobtasks/add`, data)
            console.log('response after try', response)

            let tempMsg = response.data.message.slice(0, 6)
            if(response.data.ok === false && tempMsg === 'E11000') {
                
                return alert(`Error! jobtaskID has to be unique, an jobtask with jobtaskID:${form.jobtaskID} already exists in the database.`)
            }
            else if(response.data.ok === true) {
                
                return alert(`jobtask with jobtaskID: ${form.jobtaskID} and name: ${form.jobtask_firstname} has been created in the database.`)
            }
            
            
        }catch(error){
            console.log(error)
            
          
        }
    }
    return (

        
        <div >

        <EventFindOne 
        input={input} 
        hideRender={false}/>
        
            <form onSubmit={handleSubmit} className="CRUD-form" >
                <h3>Adding a new jobtask</h3>
                <div>* = required fields</div>

                *jobtask Name: <input 
                                    required
                                    name='jobtask_firstname'
                                    onChange={handleEventFormChange}
                                    value={inputsFromSearch.event_firstname}
                                    placeholder=''/>
                jobtaskID:<input name='jobtaskID'
                                    onChange={handleFormChange}
                                    value={form.jobtaskID}
                                    placeholder=''/> 
                 {/* jobtask Type:<select onChange={handleFormChange} name='jobtask_type'>
                        <option value="default"> - choose jobtask type - </option>
                        <option value='MICE'>MICE</option>
                        <option value='Cruise'>Cruise</option>
                        </select> */}
                jobtask Type:<input name='jobtask_type'
                    onChange={handleEventFormChange}
                    value={inputsFromSearch.event_type}
                    placeholder='' />
                jobtask Image: <input name='jobtask_image'
                                    onChange={handleFormChange}
                                    value={form.jobtask_image}
                                    placeholder=''/>
                jobtask Logo: <input name='jobtask_logo'
                                    onChange={handleEventFormChange}
                                    value={inputsFromSearch.event_logo}
                                    placeholder=''/>
                jobtask Subname: <input name='jobtask_middlename'
                                    onChange={handleEventFormChange}
                                    value={inputsFromSearch.event_middlename}
                                    placeholder=''/>
                jobtask Lastname: <input name='jobtask_lastname'
                                    onChange={handleFormChange}
                                    value={form.jobtask_lastname}
                                    placeholder=''/>
                *jobtask Date: <input 
                                    // type="date"
                                    required
                                    name='jobtask_date'
                                    onChange={handleEventFormChange}
                                    value={inputsFromSearch.event_date}
                                    />
                jobtask Startdate: <input name='jobtask_startdate'
                                    // type="date"
                                    onChange={handleEventFormChange}
                                    value={inputsFromSearch.event_startdate}
                                    placeholder=''/>
                jobtask Starttime: <input name='jobtask_starttime'
                                    // type="time"
                                    onChange={handleEventFormChange}
                                    value={inputsFromSearch.event_starttime}
                                    placeholder=''/>
                jobtask Enddate: <input name='jobtask_enddate'
                                    // type="date"
                                    onChange={handleFormChange}
                                    value={form.jobtask_enddate}
                                    placeholder=''/>
                jobtask Endtime: <input name='jobtask_endtime'
                                    // type="time"
                                    onChange={handleFormChange}
                                    value={form.jobtask_endtime}
                                    placeholder=''/>
                {/* jobtask Country Location: <select required onChange={handleFormChange} name='jobtask_country_location'>
                        <option value="default">- choose country jobtask city location -</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Norway">Norway</option>
                        <option value="Estonia">Estonia</option>
                        </select> */}
                 jobtask Country Location: <input 
                                    required
                                    name='inputsFromSearch.event_location'
                                    onChange={handleEventFormChange}
                                    value={inputsFromSearch.event_location}
                                    placeholder=''/>
                jobtask City Location: <input 
                                    required
                                    name='inputsFromSearch.event_location'
                                    onChange={handleEventFormChange}
                                    value={inputsFromSearch.event_location}
                                    placeholder=''/>
                jobtask Location: <input name='jobtask_location'
                                    onChange={handleFormChange}
                                    value={form.jobtask_location}
                                    placeholder=''/>
                jobtask Adress Street: <input name='jobtask_adress_street'
                                    onChange={handleFormChange}
                                    value={form.jobtask_adress_street}
                                    placeholder=''/>
                jobtask Adress Number: <input name='jobtask_adress_number'
                                    onChange={handleFormChange}
                                    value={form.jobtask_adress_number}
                                    placeholder=''/>
                jobtask Adress Postcode: <input name='jobtask_adress_postcode'
                                    onChange={handleFormChange}
                                    value={form.jobtask_adress_postcode}
                                    placeholder=''/>
                jobtask Adress Country: <input name='jobtask_adress_country'
                                    onChange={handleFormChange}
                                    value={form.jobtask_adress_country}
                                    placeholder=''/>
                jobtask Staff Needed: <input name='jobtask_staffneeded'
                                    onChange={handleFormChange}
                                    value={form.jobtask_staffneeded}
                                    placeholder=''/>
                jobtask Jobtasks: <input name='jobtask_jobtasks'
                                    onChange={handleFormChange}
                                    value={form.jobtask_jobtasks}
                                    placeholder=''/>
                jobtask Type Of Staff Needed: <input name='jobtask_type_of_staff_needed'
                                    onChange={handleFormChange}
                                    value={form.jobtask_type_of_staff_needed}
                                    placeholder=''/>
                jobtask Staffsignedup: <input name='jobtask_staffsignedup'
                                    onChange={handleFormChange}
                                    value={form.jobtask_staffsignedup}
                                    placeholder=''/>
                jobtask Language: <input name='jobtask_language'
                                    onChange={handleFormChange}
                                    value={form.jobtask_language}
                                    placeholder=''/>
                jobtask General Notes To Staff: <input name='jobtask_general_notes_to_staff'
                                    onChange={handleFormChange}
                                    value={form.jobtask_general_notes_to_staff}
                                    placeholder=''/>
                jobtask Dutymanagers: <input name='jobtask_dutymanagers'
                                    onChange={handleFormChange}
                                    value={form.jobtask_dutymanagers}
                                    placeholder=''/>
                <button>Add new jobtask</button>
            </form>
        





        </div>
    )
}

export default JobtaskAdd