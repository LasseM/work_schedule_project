const formJobtaskContent = {

    jobtask_firstname: 'jobtaskname',
    jobtaskID: '12',
    jobtask_type: '',
    office_image: {
        photo_url: "",
        public_id: "",
    }, 
    office_logo: {
        photo_url: "",
        public_id: "",
    },
    jobtask_middlename: '$$',
    jobtask_lastname: '££',
    jobtask_date: '31.12.2019',
    jobtask_startdate: '31.12.2019',
    jobtask_starttime: '10:00',
    jobtask_enddate: '01.01.2020',
    jobtask_endtime: '€€',
    jobtask_country_location: '',
    jobtask_city_location: 'Horsens',
    jobtask_location: 'Opus Hotel',
    jobtask_adress_street: 'Hovedvej',
    jobtask_adress_number: '10',
    jobtask_adress_postcode: '7800',
    jobtask_adress_country: '',
    jobtask_staffneeded: '50',
    jobtask_jobtasks: 'Meet&Greet',
    jobtask_type_of_staff_needed: 'Guides',
    jobtask_staffsignedup: '20',
    jobtask_language: 'Spanish',
    jobtask_general_notes_to_staff: 'Get on board',
    jobtask_dutymanagers: 'Steven',

}
export default formJobtaskContent