const EmptyFormJobtaskContent = {

    _id: '',
    jobtask_firstname: '',
    jobtaskID: '',
    jobtask_type: 'something',
    office_image: {
        photo_url: "",
        public_id: "",
    }, 
    office_logo: {
        photo_url: "",
        public_id: "",
    },
    jobtask_middlename: '',
    jobtask_lastname: '',
    jobtask_date: '',
    jobtask_startdate: '',
    jobtask_starttime: '',
    jobtask_enddate: '',
    jobtask_endtime: '',
    jobtask_country_location: 'something',
    jobtask_city_location: '',
    jobtask_location: '',
    jobtask_adress_street: '',
    jobtask_adress_number: '',
    jobtask_adress_postcode: '',
    jobtask_adress_country: '',
    jobtask_staffneeded: '',
    jobtask_jobtasks: '',
    jobtask_type_of_staff_needed: '',
    jobtask_staffsignedup: '',
    jobtask_language: '',
    jobtask_general_notes_to_staff: '',
    jobtask_dutymanagers: '',    
    

}
export default EmptyFormJobtaskContent